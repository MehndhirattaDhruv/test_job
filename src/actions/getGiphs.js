;
import store from '../store';
import types from '../types';
import { getGiphyAPI } from '../api/getGiphs'
const { dispatch } = store;

export function getGiphyList(payload) {
    dispatch({
        type: types.GET_GIPHY_ITEMS
    })

    return new Promise((response, rej) => getGiphyAPI(payload)
        .then(res => {
            dispatch({
                type: types.GET_GIPHY_ITEMS_SUCCESS,
                payload: res
            })
            return response(res)

        })
        .catch(err => {
            dispatch({
                type: types.GET_GIPHY_ITEMS_FAILED
            })

            return rej(err)
        })
    )

}


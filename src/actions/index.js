import * as auth from "./auth";
import * as getEvents from "./getEvents";
import * as pushNotifications from "./pushNotifications";
import * as internetCheck from "./internetCheck";
import * as feedback from './feedback'
import * as getGiphs from './getGiphs'

export default {
	...auth,
	...getEvents,
    ...pushNotifications,
    ...internetCheck,
    ...feedback,
    ...getGiphs
};

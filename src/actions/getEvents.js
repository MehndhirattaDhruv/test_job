import {
    getMyEventsAPI,
    postEventsAPI,
    getOwnEventsAPI,
    getEventDetailAPI,
    editEventAPI,
    getEventDetailAllAPI,
    subscibeEventAPI,
    getSubscribeEventsAPI,
    getSubscribedHistoryAPI,
    getDetailOfEventHistoryAPI,
    deleteEventAPI
} from '../api/getEventsAPI';
import store from '../store';
import types from '../types';
const { dispatch } = store;

export function setIsLoaded(payload) {
    dispatch({
        type: types.SET_IS_LOADED,
        payload
    })
}

export function setScrollIndex(payload) {
    dispatch({
        type: types.SET_SCROLL_INDEX,
        payload
    });
}

export function setMyEvents(payload) {
    dispatch({
        type: types.GET_MY_EVENTS_SUCCESS,
        payload
    });
}

//action used to fetch results of all the events that is of HOMEPAGE
export function getAllEvents(search='', sort='', lat , long, start=0) {
    setMyEvents({events: []});
    setIsLoaded(false);
    dispatch({
        type: types.GET_MY_EVENTS,
        payload: {
            fetchingType: 'fetching'
        }
    })

    return new Promise((response, rej) => getMyEventsAPI(search, sort , lat , long , start )
        .then(res => {
            setMyEvents(res);
            return response(res)
          
        })
        .catch(err => {
            dispatch({
                type: types.GET_MY_EVENTS_FAILED
            })

            return rej(err)
        })
    )

}

// action used to fetch more results in homepage when user has ended its bottom scroll
export function getAllEventsFetchMore(search, sort, lat, long, start) {
    dispatch({
        type: types.GET_MY_EVENTS,
        payload: {
            fetchingType: 'loadMore'
        }
    })

    return new Promise((response, rej) => getMyEventsAPI(search, sort, lat, long, start)
        .then(res => {
            dispatch({
                type: types.GET_MY_EVENTS_LOAD_MORE,
                payload: res
            })
            if(res.status == 401) {
                setIsLoaded(true);
            }
            return response(res)
        })
        .catch(err => {
            dispatch({
                type: types.GET_MY_EVENTS_FAILED
            })
            return rej(err)
        })
    )
}

// action used to get detail of the event which is posted by the logged in user itself
export function getSingleEvent(payload) {
    dispatch({
        type: types.GET_EVENT_DETAIL
    })

    return new Promise((response, rej) => getEventDetailAPI(payload)
        .then(res => {
            dispatch({
                type: types.GET_EVENT_DETAIL_SUCCESS,
                payload: res
            })
            return response(res)

        })
        .catch(err => {
            dispatch({
                type: types.GET_EVENT_DETAIL_FAILED
            })

            return rej(err)
        })
    )
}

// action used to get detail of the event which is visible to every one 
export function getSingleEventAll(payload) {
    dispatch({
        type: types.GET_EVENT_DETAIL_ALL
    })

    return new Promise((response, rej) => getEventDetailAllAPI(payload)
        .then(res => {
            dispatch({
                type: types.GET_EVENT_DETAIL_SUCCESS_ALL,
                payload: res
            })
            return response(res)

        })
        .catch(err => {
            dispatch({
                type: types.GET_EVENT_DETAIL_FAILED_ALL
            })

            return rej(err)
        })
    )

}

//action used to get list of  his/her personal events 
export function getOwnEvents() {
    dispatch({
        type: types.GET_OWN_EVENTS
    })

    return new Promise((response, rej) => getOwnEventsAPI()
        .then(res => {
            dispatch({
                type: types.GET_OWN_EVENTS_SUCCESS,
                payload: res
            })
            return response(res)

        })
        .catch(err => {
            dispatch({
                type: types.GET_OWN_EVENTS_FAILED
            })

            return rej(err)
        })
    )

}

//action used to add events
export function postEvents(payload) {
    dispatch({
        type: types.POST_EVENTS
    })

    return new Promise((response, rej) => postEventsAPI(payload)
        .then(res => {
            dispatch({
                type: types.POST_EVENTS_SUCCESS,
                payload: res
            })
            return response(res)

        })
        .catch(err => {
            dispatch({
                type: types.POST_EVENTS_FAILED
            })

            return rej(err)
        })
    )

}

//action used to edit events
export function editEvent(payload, id) {
    dispatch({
        type: types.EDIT_EVENT
    })
    return new Promise((response, rej) => editEventAPI(payload, id)
        .then(res => {
            dispatch({
                type: types.EDIT_EVENT_SUCCESS,
                payload: res
            })
            return response(res)

        })
        .catch(err => {
            dispatch({
                type: types.EDIT_EVENT_FAILED
            })

            return rej(err)
        })
    )
}

//action used to delete event 
export function deleteEvent(id) {
    dispatch({
        type: types.DELETE_EVENT
    })
    return new Promise((response, rej) => deleteEventAPI(id)
        .then(res => {
            dispatch({
                type: types.DELETE_EVENT_SUCCESS,
                payload: res
            })
            return response(res)

        })
        .catch(err => {
            dispatch({
                type: types.DELETE_EVENT_FAILED
            })

            return rej(err)
        })
    )
}


// Actions related to handle organizing users in add event 


export const  addOrganizingUser  = (user) => {
    dispatch({
        type: types.ADD_ORGANIZERS,
        payload: user
    });

}

export const clearOrganizer = () => {
    dispatch({
        type: types.ADD_ORGANIZERS_EMPTY
    });

}



//action related to subscribe and unsubscribe the event 
export function subscribeEvent(payload) {
    dispatch({
        type: types.SUBSCRIBE_POST_EVENT
    })

    return new Promise((response, rej) => subscibeEventAPI(payload)
        .then(res => {
            dispatch({
                type: types.SUBSCRIBE_POST_EVENT_SUCCESS,
                payload: res
            })
            return response(res)

        })
        .catch(err => {
            dispatch({
                type: types.SUBSCRIBE_POST_EVENT_FAILED
            })

            return rej(err)
        })
    )

}

//action related to handle success case of getting list of subscribed events 
export function setSubscribedEvents(payload) {
    dispatch({
        type: types.GET_SUBSCRIBED_EVENTS_SUCCESS,
        payload
    })
}

//action to get all subscribed events
export function getSubscribedEvents() {
    dispatch({
        type: types.GET_SUBSCRIBED_EVENTS
    })

    return new Promise((response, rej) => getSubscribeEventsAPI()
        .then(res => {
            setSubscribedEvents(res)
            return response(res)

        })
        .catch(err => {
            dispatch({
                type: types.GET_SUBSCRIBED_EVENTS_FAILED
            })

            return rej(err)
        })
    )
}


/**** Event history actions  */

//used to get list of event history
export function getSubscribedHistory() {
    dispatch({
        type: types.GET_SUBSCRIBED_EVENTS_HISTORY
    })

    return new Promise((response, rej) => getSubscribedHistoryAPI()
        .then(res => {
            dispatch({
                type: types.GET_SUBSCRIBED_EVENTS_HISTORY_SUCCESS,
                payload: res
            })
            return response(res)

        })
        .catch(err => {
            dispatch({
                type: types.GET_SUBSCRIBED_EVENTS_HISTORY_FAILED
            })

            return rej(err)
        })
    )

}

//used to get detail of event history event 
export function getDetailOfEventHistory(payload) {
    dispatch({
        type: types.GET_SUBSCRIBED_EVENTS_HISTORY_DETAIL
    })

    return new Promise((response, rej) => getDetailOfEventHistoryAPI(payload.id)
        .then(res => {
            dispatch({
                type: types.GET_SUBSCRIBED_EVENTS_HISTORY_SUCCESS_DETAIL,
                payload: res
            })
            return response(res)

        })
        .catch(err => {
            dispatch({
                type: types.GET_SUBSCRIBED_EVENTS_HISTORY_FAILED_DETAIL
            })

            return rej(err)
        })
    )
}
  /**********************     End of event history stuff  **************/

  //special case to handle logout and clear all the events of previous  loggedin session 
 export const onLogoutHandler = () => {
    dispatch({
        type: types.ON_LOGOUT
    });

}

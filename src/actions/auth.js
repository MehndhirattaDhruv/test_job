import {
    loginAPI,
    registerAPI,
    forgotPasswordAPI,
    skipLoginAPI,
    editProfileAPI,
    toggleNotificationAPI
} from '../api/auth';
import store from '../store';
import types from '../types';
import {
    AsyncStorage
} from 'react-native';
const { dispatch } = store;

//action used to handle SUCCESS case of the login api 
export const saveUserAuth = (user) => {
    dispatch({
        type: types.AUTH_LOGIN_SUCCESS,
        payload: user
    });
}

//action for  login with email and password
export function login(payload) {
    dispatch({
        type: types.AUTH_LOGIN
    })


    return new Promise((response, rej) => loginAPI(payload)
        .then(res => {
            saveUserAuth(res)
            return response(res)
        })
        .catch(err => {
            dispatch({
                type: types.AUTH_LOGIN_FAILED
            })

            return rej(err)
        })
    )

}


//action used to handle the SUCCESS case of skip users login mechanism  
export const saveUserAuthSkip = (user) => {
    dispatch({
        type: types.SKIP_LOGIN_SUCCESS,
        payload: user
    });
}

//action used for skip login
export function skipLogin(payload) {
    dispatch({
        type: types.SKIP_LOGIN
    })


    return new Promise((response, rej) => skipLoginAPI(payload)
        .then(res => {
            saveUserAuthSkip(res)
            return response(res)
        })
        .catch(err => {
            dispatch({
                type: types.SKIP_LOGIN_FAILED
            })

            return rej(err)
        })
    )

}


// action used to registration of the user 
export function register(payload) {
    dispatch({
        type: types.AUTH_REGISTRATION
    })


    return new Promise((response, rej) => registerAPI(payload)
        .then(res => {

           dispatch({
               type: types.AUTH_REGISTRATION_SUCCESS,
               payload:res
           })
           return response(res)
        })
        .catch(err => {
            dispatch({
                type: types.AUTH_REGISTRATION_FAILED
            })

            return rej(err)
        })
    )

}

//action used for forgot password
export function forgotPassword(payload) {
    dispatch({
        type: types.FORGOT_PASSWORD
    })


    return new Promise((response, rej) => forgotPasswordAPI(payload)
        .then(res => {

            dispatch({
                type: types.FORGOT_PASSWORD_SUCCESS,
                payload: res
            })
            return response(res)
        })
        .catch(err => {
            dispatch({
                type: types.FORGOT_PASSWORD_FAILED
            })

            return rej(err)
        })
    )

}

//action used for edit profile of a logged in users 
export function editProfile(payload) {
    dispatch({
        type: types.UPDATE_PROFILE
    })


    return new Promise((response, rej) => editProfileAPI(payload)
        .then(res => {

            dispatch({
                type: types.UPDATE_PROFILE_SUCCESS,
                payload: res
            })
            return response(res)
        })
        .catch(err => {
            dispatch({
                type: types.UPDATE_PROFILE_FAILED
            })

            return rej(err)
        })
    )

}

// action used for  allowing or disallowing notification  for the events
export function changeNotification() {
    dispatch({
        type: types.CHANGE_NOTIFICATION
    })


    return new Promise((response, rej) => toggleNotificationAPI()
        .then(res => {

            dispatch({
                type: types.CHANGE_NOTIFICATION_SUCCESS,
                payload: res
            })
            return response(res)
        })
        .catch(err => {
            dispatch({
                type: types.CHANGE_NOTIFICATION_FAILED
            })

            return rej(err)
        })
    )

}





export const rememberMe = async(obj) => {
    
    try {
       await  AsyncStorage.setItem("email", obj.email)
        await AsyncStorage.setItem("password", obj.password)
    } catch (error) {
        // Error saving data
    }
    
}
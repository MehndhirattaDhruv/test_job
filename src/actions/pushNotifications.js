;
import store from '../store';
import types from '../types';
import { pushNotificationsAPI , getUserAvailabilityAPI } from '../api/pushNotificationsAPI';
const { dispatch } = store;

//action used to send notification to the subscribers of the event 
export function pushNotifications(payload) {
    dispatch({
        type: types.PUSH_NOTIFICATIONS
    })

    return new Promise((response, rej) => pushNotificationsAPI(payload)
        .then(res => {
            dispatch({
                type: types.PUSH_NOTIFICATIONS_SUCCESS,
                payload: res
            })
            return response(res)

        })
        .catch(err => {
            dispatch({
                type: types.PUSH_NOTIFICATIONS_FAILED
            })

            return rej(err)
        })
    )

}


export function UploadUserReponse(payload) {
    dispatch({
        type: types.USER_AVAILABILITY
    })

    return new Promise((response, rej) => getUserAvailabilityAPI(payload)
        .then(res => {
            dispatch({
                type: types.USER_AVAILABILITY_SUCCESS,
                payload: res
            })
            return response(res)

        })
        .catch(err => {
            dispatch({
                type: types.USER_AVAILABILITY_FAILED
            })

            return rej(err)
        })
    )

}
import {
    postFeedbackAPI,
    postFeedbackAppAPI
} from '../api/feedback';
import store from '../store';
import types from '../types';
const { dispatch } = store;



//action used to post review or feedback of the event which user has attended and it is shown in Event history 
export function postReview(payload) {
    dispatch({
        type: types.POST_REVIEW
    })


    return new Promise((response, rej) => postFeedbackAPI(payload)
        .then(res => {
          dispatch({
            type: types.POST_REVIEW_SUCCESS,
            payload: res
          });
          return response(res);
        })
        .catch(err => {
          dispatch({
            type: types.POST_REVIEW_FAILED
          });

          return rej(err);
        })
    );
}


//action used to post review or feedback of the APP
export function postReviewForAPP(payload) {
    dispatch({
        type: types.POST_REVIEW_FOR_APP
    })
    return new Promise((response, rej) => postFeedbackAppAPI(payload)
        .then(res => {
          dispatch({
            type: types.POST_REVIEW_FOR_APP_SUCCESS,
            payload: res
          });
          return response(res);
        })
        .catch(err => {
          dispatch({
            type: types.POST_REVIEW_FOR_APP_FAILED
          });

          return rej(err);
        })
    );
}


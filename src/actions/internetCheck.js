import types from '../types';
import store from '../store';

const {dispatch}=store;

//action used to handle Internet Checks 
export function updateInternetConnection(data){
    dispatch({
        type:types.CHECK_INTERNET_CONNECTION,
        payload:data
    });
}
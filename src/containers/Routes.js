import React, { Component } from "react";
import {
  createSwitchNavigator,
  createAppContainer,
  createBottomTabNavigator,
  createStackNavigator,
  createDrawerNavigator,
  DrawerItems
} from "react-navigation";
import {
  Homepage,
  AddEvents,
  AddOrganizers,
} from "./";
import { Icon } from "native-base";
import { Text, View, TouchableOpacity, Image, YellowBox , SafeAreaView, ScrollView , Dimensions} from "react-native";

const mainNav = createDrawerNavigator({
  Homepage: Homepage,
  AddEvents:AddEvents
},{
  initialRouteName:"Homepage"
}
);

const CustomDrawer = (props)=> {
  <SafeAreaView style={{flex:1 }}>
    <ScrollView>
      <DrawerItems {...props}/>
    </ScrollView>
  </SafeAreaView>

}
const Routes = createAppContainer(mainNav);

export default class extends Component {
  render() {
    return (
      <View style={{ flex: 1, fontFamily: "Lato-LightItalic" }}>
        <Routes />
      </View>
    );
  }
}

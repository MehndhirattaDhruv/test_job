//File Name: addevents.js
//Path: src/containers/events
//Description: this file consists of the ADD_EVENT with complete UI and functionality
import React from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    Dimensions,
    TextInput,
    AsyncStorage,
    KeyboardAvoidingView,
    ScrollView,
    SafeAreaView,
    TouchableWithoutFeedback,
    Keyboard,
    StatusBar,
    Alert,
    ImageBackground,
    Platform,
    BackHandler,
    Picker,
    ART,
    FilePickerManager,
    TouchableOpacity,
    
} from 'react-native';

import {
    Content,
    Button,
    Icon,
    Item,
    Input,
    Label,
    Form,
    Header,
    Container,
    Footer,
    FooterTab,
    Left,
    Right,
    Body,
    Title,
    CardItem,
    Card,
    Textarea,
    Root,
    Toast,
    DatePicker
} from 'native-base';
import { connect } from 'react-redux'
import DateTimePicker from 'react-native-modal-datetime-picker';
import ImagePicker from 'react-native-image-crop-picker';
import actions from '../../actions' 
import moment from 'moment';
import validateInput from '../../../Utilities/Validations/AddEvents'
import { AddOrganizers } from '../'
import Slider from "react-native-slider";
import { Bubbles, DoubleBounce, Bars, Pulse } from 'react-native-loader';
import Spinner from 'react-native-loading-spinner-overlay'
import { GoogleAutocomplete } from '../../components';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const today = new Date()

class AddEvents extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data:{
                image:null,
                title:"",
                description:"",
                location:"",
                start_date: moment().valueOf(),
                start_time: moment().add(1, "hour").startOf('minute').valueOf(),
                end_date: '',
                end_time: moment().startOf('minute').valueOf(),
                size:50,
                lat_long:"",
                organisers:[]
            },
            isDateTimePickerVisible: {
                start_date: false,
                end_date: false
            },
            isTimePicker: {
                start_time: false,
                end_time: false
            },
            
            errors:{},
            currentPosition: 2,

         
        }
    }

    //It is a React lifecycle method that runs just after the component is rendered.
    componentDidMount(){
        actions.clearOrganizer()
        BackHandler.addEventListener("hardwareBackPress", () => {
            if (this.props.navigation.state.routeName ==="AddEvents"){
                this.props.navigation.navigate("Homepage");
            }
            else{
                this.props.navigation.navigate("MyEvents");
            }
            return true;
        });
    }

    //It is a React lifecycle method that runs just before the  component disappear .
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress");
    }

    //FunctionName:update
    //Description:This function is  used to update the location(Name) and latitude and longitude 
    update = (input , lat) => {
        let { data  } = this.state 
        this.setState({ 
            data:{
                ...this.state.data,
                location:input,
                lat_long:lat || ""
            }
        })
    }

    //FunctionName:handleTextChange
    //Description:This function is used on the  onchange of location input to fetch more results of desirable locations
    handleTextChange = (callback) => (val) => {
        let { errors } =this.state
        this.update(val);
        callback(val);
        this.setState({
            errors:{
                ...errors, 
                location:""
            }
        })
    }

    //FunctionName:clearSearch
    //Description:This function is used to clear the location input and it is triggered when user click on button clear
    clearSearch = (callback) => (param) => {
        this.update('');
        callback();
    } 

    //FunctionName:openCamera
    //Description:This function is used to open camera with crop options while adding event 
    openCamera = ()=>{
        let { data } = this.state
         ImagePicker.openCamera({
             height: 0.28 * height,
             width: 0.94 * width,
            cropping: true,
            includeBase64: true,
            useFrontCamera: true,
            mediaType: "photo"
        }).then(image => {
            this.setState({
                data: {
                    ...data,
                    image: image
                }
            });
        });
    }

    //FunctionName:openGallery
    //Description:This function is used to open gallery for image picking option
    openGallery = ()=>{
        let { data } = this.state
        ImagePicker.openPicker({
            height: 0.28 * height,
            width: 0.94*width,
            cropping: true,
            includeBase64:true,
            useFrontCamera:true,
            mediaType:"photo"
        }).then(image => {
            this.setState({
                    data:{
                        ...data,
                        image: image
                    }
                });
        });
    }

    //FunctionName:selectPhotoTapped
    //Description:This function is called when user click onto add image section and decide whether to click image or pick from gallery
    selectPhotoTapped = () =>  {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            chooseFromLibraryButtonTitle:"Choose from Library",
            takePhotoButtonTitle:"Take Photo",
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
            },
        };
        Alert.alert(
            'Select a Photo',
            "Select a Photo for this event ",
            [
                { text: 'Take  Photo',onPress:() => this.openCamera() },
                { text: 'Choose From Library', onPress:() => this.openGallery() }
            ]
        );
    }

    //FunctionName:pushArrayValues
    //Description:This function is to add organizers of the event
    pushArrayValues = () => {
         let { data } = this.state
         data && data.organisers.push({ name: '', email: '' })
        this.setState({ 
           data
        })
    }

    //FunctionName:popValues
    //Description:This function is to remove organizers of the event
    popValues  = (index) => {
        let { data } = this.state
        data.organisers.splice(index,1)
        this.setState({
            data
        })
    }

    //FunctionName:onChangeOrganiserUsers
    //Description:This function is used in case of change in the fields of email and name of add organizers screen
    onChangeOrganiserUsers = (key , value , index) => {
        let { data = {} } = this.state;
        let { organisers } = data;
        organisers[index][key]= value;

        this.setState({
            data: {
                ...data,
                organisers
            }
        })
    }
  
    //FunctionName:onChangeUser
    //Description:This function is used in case of change in the fields of title, description etc in add event screen;
    onChangeUser = (key, val) => {
        let { data , errors } = this.state;
        if(key=="size"){
            if(val>300){
                data[key] = 300;
                this.setState({
                    data
                });
                return;
            }
            else
                data[key] = val;
            
        }
        data[key] = val;

        this.setState({
            data,
            errors: {
                ...this.state.errors,
                [key]: ''
            }
        });
    }
    
    //FunctionName:showDateTimePicker
    //Description:This function is used for opening the date picker in case of start date and end date
    showDateTimePicker = (key) => () => this.setState((prevState) => ({ isDateTimePickerVisible: {
        ...prevState.isDateTimePickerVisible,
        [key]: true 
    }}));

    //FunctionName:hideDateTimePicker
    //Description:This function is used for closing the date picker in case of start date and end date
    hideDateTimePicker = (key) => () => this.setState((prevState) => ({ isDateTimePickerVisible: {
        ...prevState.isDateTimePickerVisible,
        [key]: false 
    }}));

    //FunctionName:showTimePicker
    //Description:This function is used for opening the time picker in case of start time and end time
    showTimePicker = (key) => () => this.setState((prevState) => ({ isTimePicker: {
        ...prevState.isTimePicker,
        [key]: true
    }}));
    
    //FunctionName:hideTimePicker
    //Description:This function is used for closing  the time picker in case of start time and end time
    hideTimePicker = (key) => () => this.setState((prevState) => ({ isTimePicker: {
        ...prevState.isTimePicker,
        [key]: false
    }}));

    //FunctionName:handleDatePicked
    //Description:This function is triggered after selection of date in case of start and end dates
    handleDatePicked = (key) => (updatedDate) => {

        let { data ,errors } =this.state;
        
        let updatedData = {
            ...data,
            [key]: moment(updatedDate).valueOf()
        };

        const otherDate = updatedData[key == 'start_date' ? 'end_date' : 'start_date'];

        if(moment(otherDate).isSame(moment(updatedDate), 'day')) {
            updatedData = {
                ...updatedData,
                start_time: moment().add(1,"hour").valueOf(),
                end_time: moment().add(1, 'hour').valueOf()
            }
        }
        this.setState({ 
            data:{
                ...updatedData,
            },
            errors:{
                ...errors,
                [key]:"",
                end_time: "",
                start_time: ""
            }
        });
        this.hideDateTimePicker(key)()
    };

    //FunctionName:handleTimePicked
    //Description:This function is triggered after selection of time in case of start and end times
    handleTimePicked = (key) => (updatedDate) => {
        let { data ,errors} = this.state;
        this.setState({
            data: {
                ...data,
                [key]: moment(updatedDate).valueOf()
            },
            errors:{
                ...errors,
                [key]:""
            }
        });
        this.hideTimePicker(key)();
    };


    //FunctionName:checkValidTime
    //Description:This function is triggered to show validations for incorrect formats
    checkValidTime = () => {
        const { data } = this.state;
        const { start_date, start_time, end_time, end_date } = data || {};

        let todaysDate = moment().format("LL")
       
        let isValid = true;

        if(moment(todaysDate).isSame(moment(start_date).format('LL') , 'day')) {
            if(moment(`${moment().format('MM/DD/YYYY')} ${moment(start_time).format('hh:mm:ss A')}`).isBefore(moment().startOf('minute'))) {
                this.setState((prevState) => ({
                    errors: {
                        ...prevState.errors,
                        start_time: 'Please enter a valid start time'
                    }
                }));
                isValid = false;
            }
        }
        if(moment(todaysDate).isSame((end_date ? moment(end_date) : moment().add(1, 'day')).format('LL') , 'day')) {
            if(moment(`${moment().format('LL')} ${moment(end_time).format('hh:mm:ss A')}`).isBefore(moment().startOf('minute'))){
                this.setState((prevState) => ({
                    errors: {
                        ...prevState.errors,
                        end_time: 'Please enter a valid end time'
                    }
                }));
                isValid = false;
            }
        }
        const duration = moment.duration(moment(`${(end_date ? moment(end_date) : moment(start_date).add(1, 'day')).format('LL')} ${moment(end_time).format('h:mm:ss A')}`).diff(moment(`${moment(start_date).format('LL')} ${moment(start_time).format('h:mm:ss A')}`)));
        const minuteDiff = duration.asMinutes();
        if (minuteDiff < 0) {
            this.setState((prevState) => ({
                errors: {
                    ...prevState.errors,
                    end_time: 'Please enter a valid end time'
                }
            }));
            isValid = false;
        }

        return isValid;
    }
    
    //FunctionName:onSubmit
    //Description:This function is used to submit the response of add event  to server
    onSubmit = () =>{
        let { data  } = this.state;
        let formdata = new FormData();
        
        Keyboard.dismiss()
        formdata.append("name", data.title)
        formdata.append("description", data.description)
        formdata.append("start_date", `${moment(data.start_date).format("YYYY-MM-DD")} ${moment(data.start_time).format("HH:mm")}:00` )
        formdata.append("end_date", `${data.end_date ? moment(data.end_date).format("YYYY-MM-DD") : moment(data.start_date).add(1, 'day').format('YYYY-MM-DD') } ${moment(data.end_time).format("HH:mm")}:00`)
        formdata.append("size",parseInt(data.size).toString())
        formdata.append("lat_long",data.lat_long || "")
        formdata.append("location",data.location);
        const { organizers = []} = this.props.getEvents;
        formdata.append("organisers", JSON.stringify(organizers));   
        data && data.image && data.image.data ? formdata.append("image", data && data.image && data.image.data || null ) : null
        if(this.isValid()){
                    Toast.show({
                        text: "Event added successfully",
                        duration: 3000,
                        type: "success",
                        position: "bottom",
                        style: { marginBottom: 49, opacity: 0.9 },
                        textStyle: { fontSize: 13.5 },
                      
                    });
        }
    
    }

    //FunctionName:isValid
    //Description:This function is used to check validation for Add Event  screen
    isValid = () => {
        let { errors, isValid } = validateInput(this.state.data);
        this.setState({ errors })
        if (!isValid) {
            isValid = false;
        }

        let checkTime = this.checkValidTime(); 

        return isValid && checkTime;
    }

    render() {
        let { data, isDateTimePickerVisible = {}, isTimePicker = {}, errors, visibleModal } = this.state;
        let { organizers = [], fetchingAddEvent }=this.props.getEvents;
        
        return (
            
                <Container style={{ backgroundColor: "#fff" }}>
                    <Spinner
                        visible={fetchingAddEvent}
                        overlayColor='rgba(0, 0, 0, 0.40)'
                        customIndicator={
                        <Bubbles size={10} color="#5c2ccd" />
                        }
                    />
                    <Header style={styles.header}>
                        <StatusBar backgroundColor={fetchingAddEvent ? "#000000" : "#fff"} barStyle="dark-content" />
                        <Left  style={styles.cus_left}>
                            <Button onPress={() =>  this.props.navigation.navigate("Homepage")} transparent>
                                <Image style={styles.icon} source={require('../../../assets/arrow.png')} />
                            </Button>
                        </Left>
                        <Body style={styles.body}>
                            <Title style={styles.titleHeader}>Create Event</Title>
                        </Body>
                        <Right style={styles.cus_right}>
                            <Button disabled={fetchingAddEvent ? true : false} onPress={() => this.onSubmit()} transparent>
                                <Image  style={styles.icon} source={require('../../../assets/Tick.png')} />
                            </Button>
                        </Right>
                    </Header>

                    <Content> 
                        <TouchableOpacity onPress={this.selectPhotoTapped}>
                            {data.image===null ? 
                            <Image
                                style={styles.uploadImage}
                                resizeMode='cover'
                                source={require('../../../assets/uploadImage.png')}
                            />
                            : 
                                <Image style={styles.uploadImageDone} source={{uri: `data:image/jpeg;base64, ${data.image.data}`} }/>
                            }
                        </TouchableOpacity>

                        <Form>
                            <Item style={styles.itemInput} stackedLabel error={errors.title ? true : false} >
                                <Label style={styles.label} >Title</Label>
                                <Input
                                    style={styles.input}
                                    value={data.title}
                                    onChangeText={(text) => this.onChangeUser('title', text)}
                                    returnKeyType="next"
                                    onSubmitEditing={() => { this.Description._root.focus() }}
                                />
                            </Item>
                            <Item style={styles.itemInput} stackedLabel error={errors.description ? true : false} >
                                <Label style={styles.label} >Description</Label>
                                <Input
                                    style={styles.input}
                                    ref={c => this.Description = c}
                                    value={data.description}
                                    multiline
                                    onChangeText={(text) => this.onChangeUser('description', text)}
                                />
                            </Item>

                            <Label style={{ marginLeft: 0.03 * width, paddingTop: 10, paddingBottom: 2, marginBottom: 1,...styles.label}} >Location</Label>
                                <GoogleAutocomplete
                                    update = { this.update }
                                    handleTextChange = { this.handleTextChange}
                                    clearSearch={ this.clearSearch}
                                    data={ this.state.data}
                                    editEvent={false}
                                    editable={true}
                                    errors={errors}
                                />

                            <View style={{ flex:1 , flexDirection:"row" ,  alignItems:"center"}}>
                                <Label style={{ marginLeft: 0.03 * width, marginBottom: 2, color: "#5e2bce",fontSize: 17,  fontWeight: "bold",}} >Audience Count:</Label>
                                <Input
                                    style={styles.input}
                                    onChangeText={(num) => this.onChangeUser('size', num ? parseInt(num) : 0 )}
                                    value={data.size ? parseInt(data.size).toString() : ''}
                                    keyboardType='numeric'
                                    maxLength={3}
                                />
                            </View>    
                           
                            <View style={{ flex:1 }}>
                                <Slider
                                    value={data.size}
                                    onValueChange={value => this.setState({ data: { ...data , size:parseInt(value)} })}
                                    minimumValue={1}
                                    maximumValue={300}
                                    style={{ marginLeft: 0.03*width , marginRight :0.03*width}}
                                    minimumTrackTintColor='#efeef3'
                                    maximumTrackTintColor='#efeef3'
                                    thumbTintColor='#5831c2'
                                />
                            </View>

                            
                            <View style={{ flex:1 , flexDirection:"row"}}>
                                <Item style={styles.itemInput1} stackedLabel onPress={this.showDateTimePicker('start_date')} error={errors.start_date ? true : false}>
                                    <Label style={styles.label} onPress={this.showDateTimePicker('start_date')}>Start Date</Label>
                                    <DateTimePicker
                                        isVisible={isDateTimePickerVisible['start_date']}
                                        onConfirm={this.handleDatePicked('start_date')}
                                        onCancel={this.hideDateTimePicker('start_date')}
                                        returnKeyType="next"
                                        onSubmitEditing={() => this.showTimePicker()}
                                        minimumDate={today}
                                        maximumDate={data.end_date ? new Date(moment(data.end_date).valueOf()) : undefined}
                                    />
                                    <Text style={{ paddingTop: 15, textAlign: 'left', width: '100%' }}>{moment(data.start_date).format('LL')}</Text>
                                </Item>

                                <Item style={styles.itemInput1} onPress={this.showTimePicker('start_time')} stackedLabel error={errors.start_time ? true : false}>
                                    <Label style={styles.label}>Start Time</Label>
                                    <DateTimePicker
                                        isVisible={isTimePicker['start_time']}
                                        onConfirm={this.handleTimePicked('start_time')}
                                        onCancel={this.hideTimePicker('start_time')}
                                        mode="time"
                                        is24Hour={true}
                                    />
                                    <Text style={{ paddingTop: 15, textAlign: 'left', width: '100%' }}>{moment(data.start_time).format("HH:mm")}</Text> 
                                </Item>
                            </View>

                            <View style={{ flex: 1, flexDirection: "row" , marginBottom: 20}}>
                                <Item style={styles.itemInput1} stackedLabel onPress={this.showDateTimePicker('end_date')} error={errors.end_date ? true : false}>
                                    <Label style={styles.label} onPress={this.showDateTimePicker('end_date')} >End  Date</Label>
                                    <DateTimePicker
                                        isVisible={isDateTimePickerVisible['end_date']}
                                        onConfirm={this.handleDatePicked('end_date')}
                                        onCancel={this.hideDateTimePicker('end_date')}
                                        returnKeyType="next"
                                        onSubmitEditing={() => this.showTimePicker()}
                                        minimumDate={new Date(data.start_date) || today }
                                    />
                                    <Text style={{ paddingTop: 15, textAlign: 'left', width: '100%' }}>{data.end_date ? moment(data.end_date).format('LL') : moment(data.start_date).add(1, 'day').format('LL')}</Text>
                                </Item>

                                <Item style={styles.itemInput1} onPress={this.showTimePicker('end_time')} stackedLabel error={errors.end_time ? true : false}>
                                    <Label style={styles.label}>End Time</Label>
                                    <DateTimePicker
                                        isVisible={isTimePicker['end_time']}
                                        onConfirm={this.handleTimePicked('end_time')}
                                        onCancel={this.hideTimePicker('end_time')}
                                        mode="time"
                                        is24Hour={true}
                                    />
                                    <Text style={{ paddingTop: 15, textAlign: 'left', width: '100%' }}>{moment(data.end_time).format("HH:mm")}</Text>
                                </Item>
                            </View>
                        </Form>
                    </Content>
                </Container>
              
        )
    }
}


const styles = StyleSheet.create({
    header: {
        backgroundColor: "#fff",
        shadowColor: 'transparent',
        elevation: 0,
        display: "flex",
        justifyContent: "center",
        shadowRadius: 0,
        borderColor:"#fff",
        shadowOpacity: 0,
        borderBottomWidth: 0,
        shadowOffset: {
            height: 0,
        }
    },
    titleHeader: {
        color: "#0e0e0e",
        fontSize: 18,
        fontWeight: "bold",
        width: "100%",
        textAlign: "center",
        fontFamily: "Ubuntu"
    },
    body: {
        flex: 4,
        textAlign: "center",
        justifyContent: "center"
    },
    cus_left: {
        flex: 1,
    },
    cus_right: {
        flex: 1,
    },
    icon: {
        height: 26,
        width: 26,
        resizeMode: "contain"
    },
 
    uploadImage:{
        paddingTop:8,
        height:0.28*height,
        width:"94%",
        marginLeft:0.03*width,
        marginRight:0.03*width,
        resizeMode:"contain",
        flex:1,
        borderRadius:4
    },
    uploadImageDone: {
        paddingTop: 8,
        height: 0.28 * height,
        width: "94%",
        marginLeft: 0.03 * width,
        marginRight: 0.03 * width,
        resizeMode: "cover",
        borderRadius: 4
    },
    ViewImage:{
        height:0.33*height,
        width:"100%",
        justifyContent: "center",
        alignItems: "center",

    },

    label: {
        color: "#5e2bce",
        fontSize: 17,
        fontWeight:"bold",
        paddingRight: 0.025 * width,
    },
    input: {
        color: "#191919",
        fontSize:15
    },
    itemInput: {
        paddingTop:10,
        marginLeft: 0.03 * width,
        marginRight: 0.03 * width
    },
    itemInput1: {
        flex:1,
        paddingTop: 10,
        marginLeft: 0.03 * width,
        marginRight: 0.03 * width
    },
    loginButton: {
        backgroundColor: '#dfd2fe',
        marginRight: 0.03 * width,
        padding: 10,
        marginTop: 15,
        marginBottom: 15,
        borderRadius: 20,
        // width: "60%",
        justifyContent: 'center',
        alignItems: 'center',
        height: 35,
    },
    loginTextBottom: {
        color: '#5e2bce',
        padding:5,
        textAlign: "center",
        width: '100%',
        fontSize: 16,
        fontWeight: "600"
    },
    modalContent: {
        flex: 1,
        backgroundColor: 'white',
        padding: 2,
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
})


export default connect(state => state)(AddEvents);


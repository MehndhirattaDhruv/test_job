//File Name: addorganizwers.js
//Path: src/containers/events
//Description: this file consists of the ADD_ORGANIZERS complete UI and functionality it is used to add/delete organizer
import React from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    Dimensions,
    TextInput,
    AsyncStorage,
    KeyboardAvoidingView,
    ScrollView,
    SafeAreaView,
    TouchableWithoutFeedback,
    keyboard,
    StatusBar,
    Alert,
    ImageBackground,
    Platform,
    BackHandler,
    Picker,
    FilePickerManager,
    TouchableOpacity
} from 'react-native';

import {
    Content,
    Button,
    Icon,
    Item,
    Input,
    Label,
    Form,
    Spinner,
    Header,
    Container,
    Footer,
    FooterTab,
    Left,
    Right,
    Body,
    Title,
    CardItem,
    Card,
    Textarea,
    DatePicker
} from 'native-base';
import { connect } from 'react-redux'
import Validator from 'is_js';
import actions from '../../actions'
import validateInput from '../../../Utilities/Validations/AddOrganizers'
const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const today = new Date()

class AddOrganizers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            organizers: [{ name :"" , email:""}],
            errors: [{ name: false, email: false }]
        } 
    }

    //It is a React lifecycle method that runs just after the component is rendered.
    componentDidMount() {
        let { organizers = [] } = this.props.getEvents
        if(organizers.length){
            this.setState({ organizers:[...organizers] })
        }
    }

    //FunctionName:pushArrayValues
    //Description:This function is  triggered on click of add icon to add one more row in add organizer screen
    pushArrayValues = () => {
       let { organizers }= this.state;
        let { isValid, errors } = this.validateOrganizers();
        if(isValid) {
            organizers.push({ name: '', email: '' })
        }

        this.setState({
            organizers,
            errors
        })
     
    }

    //FunctionName:validateOrganizers
    //Description:This function is  to evaluate validations in add organizers row 
    validateOrganizers = () => {
        const { organizers=[] } = this.state;
        let isValid = true;
        const errors = organizers.map(organizer => {
            if (!organizer.name || !organizer.email || !Validator.email(organizer.email) ) {
                isValid = false
            }
            return { name: !organizer.name ? true : false, email: !organizer.email || !Validator.email(organizer.email) ? true : false}
        });
        return {isValid, errors};
    }

    //FunctionName:popValues
    //Description:This function is  used to remove the field  when one clicks on delete icon
    popValues = (index) => {
        let { organizers } = this.state
        organizers.splice(index, 1)
        this.setState({
            organizers
        })
    }

    //FunctionName:onChangeOrganiserUsers
    //Description:This function is  triggered on change of email , name fields 
    onChangeOrganiserUsers = (key, value, index) => {
        let { organizers=[]  , errors} = this.state;
        organizers[index][key] = value;
        this.setState({
            organizers,
            errors: {
                ...this.state.errors,
                [key]: ''
            }
        })
    }

    //FunctionName:onSubmit
    //Description:This function handles an action which receives array of organizers
    onSubmit = () => {
        const { organizers } = this.state;
        const { isValid, errors } = this.validateOrganizers(organizers);
        if(isValid) {
            actions.addOrganizingUser(this.state.organizers)
           this.props.navigation.goBack(null)
        }
        this.setState({
            errors
        });
    }

    render() {
       let { organizers , errors } = this.state
        return (
            <Container style={{ backgroundColor: "#fff" }}>
                <Header style={styles.header}>
                    <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                    <Left style={styles.cus_left}>
                        <Button onPress={() => this.props.navigation.pop()} transparent>
                            <Image style={styles.icon} source={require('../../../assets/cross_icon.png')} />
                        </Button>
                    </Left>
                    <Body style={styles.body}>
                    </Body>
                    <Right style={styles.cus_right}>
                        <Button onPress={this.onSubmit} transparent>
                            <Image style={styles.icon} source={require('../../../assets/Tick.png')} />
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <Form>
                       { 
                        organizers.length ? organizers.map((item ,index) => {
                                return (<View style={{ flex: 1, flexDirection: "row" }}>
                                    <Item style={styles.itemInput} stackedLabel error={errors[index] && errors[index].name ? true : false}>
                                        <Input
                                            value={item.name}
                                            style={{ width: "90%" }}
                                            maxLength={30}
                                            placeholder="Name"
                                            onChangeText={(text) => this.onChangeOrganiserUsers('name', text, index)}
                                            autoFocus
                                        />
                                    </Item>

                                    <Item style={styles.itemInput} stackedLabel error={errors[index] && errors[index].email ? true : false} >
                                        <Input
                                            placeholder="Email"
                                            style={{width : "90%"}}
                                            maxLength={40}
                                            value={item.email}
                                            onChangeText={(text) => this.onChangeOrganiserUsers('email', text, index)}

                                        />
                                    </Item>
                                    <Button transparent style={{ alignSelf: "flex-end", margin: 4  }} onPress={() => this.popValues(index)}>
                                        <Image style={styles.imageIcon} source={require('../../../assets/trash.png')} />
                                    </Button>
                                </View>
                                )
                                })   
                            : null
                        }    
                    </Form>
                    <View style={{ flex:1 , justifyContent:"center"  , flexDirection:"row" , paddingTop:20}}>
                        <TouchableOpacity style={styles.viewicon} onPress={this.pushArrayValues}>
                            <View style={{ justifyContent: "center", alignItems: "center", alignContent: "center", height: 52, width: 52, borderRadius: 30,}} >
                                <Icon name="add" style={styles.viewAddicon} />
                            </View>
                        </TouchableOpacity>
                    </View>
                </Content>        
            </Container>
                
        )
    }
}


const styles = StyleSheet.create({
    header: {
        backgroundColor: "#fff",
        shadowColor: 'transparent',
        elevation: 0,
        display: "flex",
        alignItems: "flex-end",
        justifyContent: "flex-start",
        shadowRadius: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
        borderColor: "#fff",
        shadowOffset: {
            height: 0,
        }
    },
    titleHeader: {
        color: "#0e0e0e",
        fontSize: 18,
        fontWeight: "bold",
        width: "100%",
        textAlign: "center",
        fontFamily: "Ubuntu"
    },
    body: {
        flex: 4,
        textAlign: "center",
        justifyContent: "center"
    },
    cus_left: {
        flex: 1,
    },
    cus_right: {
        flex: 1,
    },
    icon: {
        height: 21,
        width: 21,
        resizeMode: "contain",
    },
    imageIcon:{
        height:16,
        width:16,
        resizeMode:"contain",
        marginRight: 5
    },
    input: {
        color: "#191919",
        fontSize: 15
    },
    itemInput: {
        paddingTop: 10,
        flex:1,
        marginLeft: 0.03 * width,
        marginRight: 0.03 * width
    },
    viewAddicon: {
        fontSize: 30,
        color: "#fff"
    },
    viewicon: {
        backgroundColor: "#5e2bce",
        borderRadius: 30,
        width: 52,
        height: 52,
        justifyContent: "center"

    },
})


export default connect(state => state)(AddOrganizers);


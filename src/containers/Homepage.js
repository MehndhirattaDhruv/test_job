//File Name: homepage.js
//Path: src/containers
//Description: this file shows list of all events posted by everyone 
import React from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    Dimensions,
    TextInput,
    AsyncStorage,
    KeyboardAvoidingView,
    ScrollView,
    SafeAreaView,
    TouchableWithoutFeedback,
    Keyboard,
    StatusBar,
    Platform,
    TouchableOpacity,
    NetInfo,
    RefreshControl,
    FlatList,
    ActivityIndicator,
    Linking,
    Alert,
    BackHandler
} from "react-native";
import {
    Content,
    Button,
    Icon,
    Item,
    Input,
    Label,
    Form,
    Spinner,
    Header,
    Container,
    Footer,
    FooterTab,
    Left,
    Right,
    Body,
    Title,
    CardItem,
    Card,
    List,
    Toast,
    Root,
    ListItem
} from 'native-base';
import { connect } from 'react-redux'
import actions from '../actions'
import moment from 'moment';
import Icons from 'react-native-vector-icons/FontAwesome';
import Popover from 'react-native-popover-view'
import { ContentLoaderCards, LoadingImage } from '../components'
import { withNavigationFocus , DrawerActions } from 'react-navigation';
import SplashScreen from 'react-native-splash-screen'
const eventsData = require('./events.json');

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
let items = [
    { label: 'Date', key: "date", id: 1 },
    { label: 'Name', key: "name", id: 2 },
    { label: 'Nearby', key: "location", id: 3 },
];

class Homepage extends React.Component {
    constructor(props) {
        super(props);
        this.handleBackPress = this.handleBackPress.bind(this);
        this.state = {
            searchTerm: "",
            errors: {},
            isVisible: false,
            selected: null,
            selectedKey: '',
            initialLoading: true,
            delayTimeout: null,
            latitude: '',
            longitude: '',
            error: null,
            start: 0,
            limit: 20,
            refreshing: false,
            id: null,
            tooltipObject: {
                addicon: false,
                accountSettings: false,
                filter: false
            }
        }
        this.viewabilityConfig = {
            waitForInteraction: true,
            viewAreaCoveragePercentThreshold: 95
        }
    }

    //It is a React lifecycle method that runs just after the component is rendered.
    componentDidMount() {
        this.checkConnectivity();
        SplashScreen.hide()
        console.log(eventsData , "eventsData")
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress() {
        console.log("back handler  enter")
        BackHandler.exitApp();
        return true;  
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    //It is a React lifecycle method that runs just when component need to updated after Mounting the screen.
    componentDidUpdate(prevProps, prevState) {
        let internetConnetion = this.props.internetCheck.internetConnection;
        let p_internetConnection = prevProps.internetCheck.internetConnection;
        if (p_internetConnection != internetConnetion && internetConnetion) {
            this.getApiCall();
        }
    }

    //FunctionName:getApiCall
    //Description:This function is used to fetch all events and ask for allow location alert 
    getApiCall = () => {
        this.setState({
            initialLoading: false
        });
        let { searchTerm, selectedKey, latitude, longitude, start } = this.state
        actions.getAllEvents(searchTerm, selectedKey, latitude, longitude, start).then(res => {
            this.setState({ initialLoading: false })
        })
            .catch(err => {
                this.setState({ initialLoading: false })
            })
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });
            },
            (error) => {
                this.setState({ error: error.message })


            },
            { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
        );
    }

    //FunctionName:handleConnectivityChange
    //Description:This function is used to handle Internet connections and show alert according to it 
    handleConnectivityChange = connectionInfo => {
        actions.updateInternetConnection(connectionInfo);
    };

    //FunctionName:checkConnectivity
    //Description:This function which is used to detect whether internet is ON/OFF
    checkConnectivity = () => {
        NetInfo.getConnectionInfo().then(connectionInfo => {
            actions.updateInternetConnection(connectionInfo);
            if (connectionInfo.type == "none") {
                Toast.show({
                    text: "No Internet Connection",
                    duration: 2000,
                    type: "danger",
                    position: "bottom",
                    style: { marginBottom: 49, opacity: 0.9 },
                    textStyle: { fontSize: 13.5 },
                });
                this.setState({ initialLoading: false })
            } else {
                this.setState({ initialLoading: false })
                this.getApiCall()
            }
        });

        NetInfo.addEventListener("connectionChange", this.handleConnectivityChange);
    };

    //FunctionName:_onRefresh
    //Description:This function used to refresh its content when users pull down refresh from top
    _onRefresh = () => {
        const { searchTerm, selectedKey, latitude, longitude, start } = this.state;
        this.setState({
            refreshing: true,
            searchTerm: '',
            selectedKey: '',
            start: 0
        });
        actions.getAllEvents(searchTerm || '', selectedKey || '', latitude, longitude, 0).then(res => {
            this.setState({
                refreshing: false
            });
        })
            .catch(err => {
                this.setState({
                    refreshing: false
                });
            })
    }

    //FunctionName:onChangeUser
    //Description:This function is triggered on change of live search and reflect the result according to it 
    onChangeUser = (val) => {
        this.setState({
            searchTerm: val
        });

        let { searchTerm, latitude, longitude, selectedKey, start } = this.state;
        if (val.length > 2) {
            var delay = 0;
            delay = 300;
            clearTimeout(this.state.delayTimeout);
            let delayTimeout = setTimeout(() => {
                actions.getAllEvents(val, selectedKey, latitude, longitude, 0).then(res => {
                })
                    .catch(err => {
                    })
            }, delay);

            this.setState({
                delayTimeout,
                start: 0
            })
        }
        else if (!val) {
            clearTimeout(this.state.delayTimeout);
            actions.getAllEvents("", selectedKey, latitude, longitude, 0).then(res => {
            })
                .catch(err => {
                })
        }
    }

    //FunctionName:showPopover
    //Description:This function is triggered to ON/OFF the popover on the right hand side to apply filters 
    showPopover() {
        this.setState({ isVisible: !this.state.isVisible });
    }

    //FunctionName:closePopover
    //Description:This function is triggered to OFF the popover on the right hand side to apply filters 
    closePopover = () => {
        this.setState({ isVisible: false });
    }

    //FunctionName:onChangeSelectedList
    //Description:This function is used to  change the options of filter  and also check location is allowed or not 
    onChangeSelectedList = (id, key) => {
        let { searchTerm, longitude, latitude, selectedKey, start, error } = this.state
        this.setState({ isVisible: false }, () => {
            ;
            if (!longitude && key == "location") {
                Toast.show({
                    text: "Please turn on your location and refresh the app.",
                    duration: 2000,
                    type: "danger",
                    position: "bottom",
                    style: { marginBottom: 49, opacity: 0.9 },
                    textStyle: { fontSize: 13.5 },
                });
                return;
            }
            this.setState({
                selected: id,
                selectedKey: key,
                start: 0
            })
        });
    }


    //FunctionName:clearActions
    //Description:This function is used to clear all applied filters and brings back fresh data 
    clearActions = () => {
        let { searchTerm, selectedKey, latitude, longitude, start } = this.state
        this.setState({
            selected: null,
            selectedKey: '',
            isVisible: false,
            searchTerm: "",
            start: 0
        })
        actions.getAllEvents("", "", latitude, longitude, 0);
    }

    //FunctionName:clearSearchTerm
    //Description:This function is used on click of cross icon of searchbar
    clearSearchTerm = () => {
        let { searchTerm, selectedKey, latitude, longitude, start } = this.state
        clearTimeout(this.state.delayTimeout);
        this.setState({
            searchTerm: '',
            start: 0
        })
    }


    //FunctionName:onViewableItemsChanged
    //Description:This function is used to keep a track of last scrolled card so that in changing tabs it does not loose the card where he/she left off
    onViewableItemsChanged = ({ viewableItems, changed }) => {
        actions.setScrollIndex(viewableItems[0] && viewableItems[0].index || 0)
    }

    //It is a React lifecycle method that evaulates and compare the  previous props with the next one and call componentDidupdate
    componentWillReceiveProps(nextProps) {
        const { scrollIndex, events = [] } = nextProps.getEvents;
        if (nextProps.isFocused && nextProps.isFocused != this.props.isFocused && events[scrollIndex]) {
            this.eventList && this.eventList.scrollToIndex({ index: scrollIndex || 0, animated: false });
        }
    }

    //FunctionName:_listEmptyComponent
    //Description:This function is used to show UI and content when there are no events 
    _listEmptyComponent = () => {
        return (
            <ScrollView style={{ flex: 1 }}>
                <View style={{ flex: 1, height: 1 * height }}>
                    <Image source={require('../../assets/subscribeEvent.png')} style={styles.forgotImage} />
                    <Text style={{ paddingTop: 15, ...styles.textHeading }}>No Events Found </Text>
                </View>
            </ScrollView>
        )
    }

    //FunctionName:onLongPressHandler
    //Description:This function is used to show tooltip on long press
    onLongPressHandler = (key) => () => {
        let { tooltipObject } = this.state
        this.setState({
            tooltipObject: {
                ...tooltipObject,
                [key]: !tooltipObject.key
            },
        });
    }

    //FunctionName:onClosePopover
    //Description:This function is used to close tooltip on long press
    onClosePopover = (key) => () => {
        let { tooltipObject } = this.state
        this.setState({
            tooltipObject: {
                ...tooltipObject,
                [key]: false
            }
        })
    }

    render() {
        let { searchTerm, selected, initialLoading, isVisible, latitude, longitude, refreshing, tooltipObject } = this.state
        let { events = [], fetching, fetchingSubscribe, total_Events, loadMore = false, isLoaded = false } = this.props.getEvents
        let { user } = this.props.auth;

        return (
            <Container style={{ backgroundColor: "#fff" }} onPress={this.closePopover}>
                <Header style={styles.header}>
                    <StatusBar backgroundColor={isVisible ? "#bfbfbf" : "#fff"} barStyle="dark-content" />
                    <TouchableOpacity ref={ref => this.accountSettings = ref} onLongPress={this.onLongPressHandler('accountSettings')}>
                            <Icon onPress={() => this.props.navigation.dispatch(DrawerActions.toggleDrawer())} name='menu' />
                        <Popover
                            fromView={this.accountSettings}
                            placement='right'
                            popoverStyle={{ "borderRadius": 5, backgroundColor:"#d1bbf9" }}
                            arrowStyle={{ backgroundColor:"#d1bbf9" , color:"#d1bbf9"}}
                            isVisible={tooltipObject.accountSettings}
                            onClose={this.onClosePopover('accountSettings')}
                        >
                        <Text style={{ padding: 15 ,color:"#fff" , fontSize:16 }}>Account Settings</Text>
                        </Popover>
                        </TouchableOpacity>
                    <Body style={styles.body}>
                        <Title style={styles.titleHeader}>All Events</Title>
                    </Body>
                        <TouchableOpacity  ref={ref => this.touchable = ref} onLongPress={this.onLongPressHandler('touchable')} onPress={() => { this.showPopover() }}>
                            <Image style={{paddingHorizontal:20 , ...styles.icon}} source={require('../../assets/filter.png')} />
                            <Popover
                                fromView={this.touchable}
                                placement='left'
                                popoverStyle={{ "borderRadius": 5, backgroundColor: "#d1bbf9" }}
                            arrowStyle={{ backgroundColor: "#d1bbf9", color: "#d1bbf9" }}
                                isVisible={tooltipObject.touchable}
                                onClose={this.onClosePopover('touchable')}
                            >
                                <Text style={{ padding: 15, color:"#fff" , fontSize: 16}}>Apply Filter</Text>
                            </Popover>
                        </TouchableOpacity>
                        <Popover
                            isVisible={isVisible}
                            fromView={this.touchable}
                            onClose={() => this.showPopover()}
                            showBackground={false}
                            placement='bottom'
                            popoverStyle={{ "borderRadius": 5, "elevation": 10 }}
                            style={{ position: 'absolute' }}>
                            <View style={{ width: 0.33 * width }}>
                                <View style={{ flex: 1, flexDirection: "row", height: 40, alignItems: "center" }}>
                                    <Text style={{ flex: 1, color: "", fontSize: 15, fontWeight: "500", paddingLeft: 5 }}>Sort By</Text>
                                    <Text onPress={this.clearActions} style={{ flex: 1, textAlign: "right", color: "#949494", paddingRight: 10, fontSize: 12 }}>Clear all</Text>
                                </View>
                                <List>
                                    {
                                        items && items.map(item =>
                                            <ListItem onPress={(text) => this.onChangeSelectedList(item.id, item.key)} selected={selected === item.id} key={item.id} style={{ backgroundColor: selected === item.id ? "#f2eaff" : '#fff', marginLeft: 0, paddingLeft: 10, height: 45 }}>
                                                <Text style={styles.TextListItem}>{item.label}</Text>
                                            </ListItem>
                                        )}
                                </List>
                            </View>

                        </Popover>
                </Header>
                <Item rounded style={styles.searchbar}>
                    <Input
                        style={styles.searchInput}
                        placeholder='Search Events'
                        value={searchTerm}
                        onChangeText={(text) => this.onChangeUser(text)}
                    />
                    {!searchTerm ? <Icon name='search' /> : <Icon name='close' onPress={this.clearSearchTerm} />}
                </Item>
                {user && user.info && user.info.is_login ? <TouchableOpacity ref={ref => this.addicon = ref} onLongPress={this.onLongPressHandler('addicon')} activeOpacity={1} style={styles.viewicon} onPress={() => { this.props.navigation.navigate('AddEvents') }}>
                    <Icon name="add" style={styles.viewAddicon} />
                    <Popover
                        fromView={this.addicon}
                        placement='left'
                        popoverStyle={{ "borderRadius": 5, backgroundColor: "#d1bbf9" }}
                        arrowStyle={{ backgroundColor: "#d1bbf9", color: "#d1bbf9" }}
                        isVisible={tooltipObject.addicon}
                        onClose={this.onClosePopover('addicon')}
                    >
                        <Text style={{ padding: 15,color:"#fff" , fontSize: 16 }}>Add Events</Text>
                    </Popover>
                </TouchableOpacity> : null}

                {!initialLoading ? !fetching ? <FlatList
                    keyExtractor={(item, index) => { return item.id }}
                    ref={(c) => this.eventList = c}
                    onViewableItemsChanged={this.onViewableItemsChanged}
                    viewabilityConfig={this.viewabilityConfig}
                    ListFooterComponent={<View style={{ marginBottom: 40 }}>{loadMore ? <ActivityIndicator color="#5e2bce" size="large" /> : null}</View>}
                    onEndReachedThreshold={0.2}
                    refreshing={refreshing}
                    onRefresh={this._onRefresh}
                    ListEmptyComponent={this._listEmptyComponent}
                    data={eventsData.events}
                    renderItem={({ item, index }) => <TouchableWithoutFeedback activeOpacity={0.1}  key={item.id}>
                        <Card transparent noShadow={true} style={styles.card}>
                            <View style={{ flex: 1, flexDirection: "row" }} >
                                <CardItem style={{ flex: 3, flexDirection: "column", padding: 0, margin: 0 }}>
                                    <View style={{
                                        borderWidth: 1,
                                        borderRadius: 5,
                                        borderColor: '#f1f1f1',
                                        shadowColor: '#f1f1f1',
                                        shadowOffset: { width: 3, height: 3 },
                                        shadowOpacity: 0.8,
                                        shadowRadius: 5,
                                        elevation: 7,
                                    }}>
                                        <LoadingImage style={styles.images} source={item.image} />
                                    </View>
                                </CardItem>

                                <CardItem style={styles.cardItem}>
                                    <Text style={styles.title} numberOfLines={1}  >{item.name}</Text>
                                    <Text style={styles.description}>{item.description.length > 45 ? item.description.substr(0, 45).trim() + '[...]' : item.description}</Text>
                                    <View style={{ flex: 1, display: "flex", flexDirection: "row", alignSelf: "flex-start", paddingBottom: 1, alignItems: "center" }}>
                                        <Image style={{ height: 18, width: 18, resizeMode: "contain" }} source={require('../../assets/location.png')} />
                                        <Text numberOfLines={1} style={styles.location}>{item.location}</Text>
                                    </View>
                                    <View style={{ flex: 1, display: "flex", flexDirection: "row", alignSelf: "flex-start", alignItems: "center" }}>
                                        <Image style={{ height: 18, width: 18, resizeMode: "contain" }} source={require('../../assets/calendar.png')} />
                                        <Text style={styles.location}>{item.start_date} - {item.end_date}</Text>
                                    </View>
                                     <Button disabled={fetchingSubscribe ? true : false} style={{ backgroundColor: item.is_subscribed ? "#5829d7" : "#e1d1ff", ...styles.buttonTop }} success><Text style={{ color: item.is_subscribed ? "#fff" : "#5e2bce", ...styles.textsubscribe }}>{item.is_subscribed ? "UNSUBSCRIBE" : "SUBSCRIBE"}</Text></Button>
                                </CardItem>
                            </View>
                        </Card>
                    </TouchableWithoutFeedback>}
                /> : <ContentLoaderCards
                        count={4}
                    /> :
                    <View style={{ marginTop: "50%" }}>
                        <ActivityIndicator size="large" color="#5e2bce" />
                    </View>
                }
            </Container>
        )
    }
}


const styles = StyleSheet.create({
    header: {
        backgroundColor: "#fff",
        alignItems: 'center',
        shadowColor: 'transparent',
        elevation: 0,
        display: "flex",
        justifyContent: "center",
        borderColor: "#fff",
        borderBottomWidth: 0,
        shadowRadius: 0,
        shadowOpacity: 0,
        shadowOffset: {
            height: 0,
        }
    },
    titleHeader: {
        color: "#0e0e0e",
        fontSize: 18,
        fontWeight: "bold",
        width: "100%",
        textAlign: "center",
        fontFamily: "Ubuntu"
    },
    body: {
        flex: 4,
        textAlign: "center",
        justifyContent: "center"
    },
    cus_left: {
        flex: 1,
    },
    cus_right: {
        flex: 1
    },
    icon: {
        height: 23,
        width: 23,
        resizeMode: "contain"

    },
    searchbar: {
        marginTop: 0.01 * height,
        marginBottom: 0.01 * height,
        marginLeft: 0.05 * width,
        marginRight: 0.05 * width,
        height: 0.055 * height,
        backgroundColor: "#f0f0f8"
    },
    images: {
        height: 0.215 * height,
        width: 0.332 * width,
        borderRadius: 5,
    },
    card: {
        marginLeft: 0.0392 * width,
        marginRight: 0.0392 * width,
        marginTop: 0.015 * height,
        marginBottom: 0.015 * height,
        borderRadius: 3,
        zIndex: 99,
    },
    cardItem: {
        flex: 5,
        display: "flex",
        flexDirection: "column",
    },
    button: {
        textAlign: "center",
        justifyContent: "center",
        backgroundColor: "#e1d1ff",
        borderColor: "transparent",
        borderRadius: 20,
        padding: 5,
        height: 30,
        width: 70,

    },
    textsubscribe: {
        fontWeight: "bold",
        fontSize: 9,
        textAlign: "center",
    },
    title: {
        fontSize: 15,
        fontWeight: "bold",
        textAlign: "left",
        color: "#0e0e0e",
        width: "100%"
    },
    description: {
        fontSize: 13,
        paddingTop: 3,
        paddingBottom: 3,
        marginBottom: 1,
        justifyContent: "flex-start",
        textAlign: "left",
        color: "#414141",
        width: "100%",
        fontWeight: "normal",
        flexWrap: 'wrap',
        flex: 2
    },
    location: {
        fontSize: 10.5,
        paddingTop: 3,
        color: "#414141",
        fontWeight: "normal",
        color: "#424242",
        paddingLeft: 10,
    },
    viewAddicon: {
        fontSize: 30,
        color: "#fff"
    },
    viewicon: {
        backgroundColor: "#5e2bce",
        borderRadius: 30,
        position: "absolute",
        right: 20,
        bottom: 25,
        zIndex: 99,
        width: 52,
        height: 52,
        elevation: 10,
        alignItems: "center",
        justifyContent: "center"

    },
    searchInput: {
        marginLeft: 10,
        fontSize: 14.5
    },
    TextListItem: {
        color: "#0d0f0e",
        fontSize: 15,
        fontWeight: "400",

    },
    buttonTop: {
        textAlign: "right",
        justifyContent: "center",
        alignItems: "center",
        height: 25,
        lineHeight: 25,
        borderRadius: 14,
        width: 0.26 * width,
        marginTop: 4
    },
    forgotImage: {
        marginTop: 0.11 * height,
        resizeMode: "contain",
        height: 160,
        width: 160,
        alignSelf: "center",
        alignContent: "center"
    },
    textHeading: {
        fontSize: 18,
        fontWeight: "600",
        textAlign: "center",
        justifyContent: "center",
        color: "#0e0e0e"

    },
})


export default connect(state => state)(withNavigationFocus(Homepage));

//types used for posting a review in case of Event History
export const POST_REVIEW = "POST_REVIEW";
export const POST_REVIEW_SUCCESS = "POST_REVIEW_SUCCESS";
export const POST_REVIEW_FAILED = "POST_REVIEW_FAILED";

//types used for posting a review of APP
export const POST_REVIEW_FOR_APP = "POST_REVIEW_FOR_APP";
export const POST_REVIEW_FOR_APP_SUCCESS = "POST_REVIEW_FOR_APP_SUCCESS";
export const POST_REVIEW_FOR_APP_FAILED = "POST_REVIEW_FOR_APP_FAILED";
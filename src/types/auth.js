// types used for login with email and password
export const AUTH_LOGIN = 'AUTH_LOGIN';
export const AUTH_LOGIN_SUCCESS = 'AUTH_LOGIN_SUCCESS';
export const AUTH_LOGIN_FAILED = 'AUTH_LOGIN_FAILED';

//types used for skip session handling
export const SKIP_LOGIN = 'SKIP_LOGIN';
export const SKIP_LOGIN_SUCCESS = 'SKIP_LOGIN_SUCCESS';
export const SKIP_LOGIN_FAILED = 'SKIP_LOGIN_FAILED';

//type used for the registration proccess
export const AUTH_REGISTRATION = 'AUTH_REGISTRATION';
export const AUTH_REGISTRATION_SUCCESS ="AUTH_REGISTRATION_SUCCESS";
export const AUTH_REGISTRATION_FAILED = 'AUTH_REGISTRATION_FAILED'


//types used in case of forgot password
export const FORGOT_PASSWORD = "FORGOT_PASSWORD"
export const FORGOT_PASSWORD_SUCCESS = 'FORGOT_PASSWORD_SUCCESS'
export const FORGOT_PASSWORD_FAILED = 'FORGOT_PASSWORD_FAILED'


//types used to update the profile of a logged in user
export const UPDATE_PROFILE = "UPDATE_PROFILE"
export const UPDATE_PROFILE_SUCCESS = 'UPDATE_PROFILE_SUCCESS'
export const UPDATE_PROFILE_FAILED = 'UPDATE_PROFILE_FAILED'


//types used to toggle notification
export const CHANGE_NOTIFICATION = 'CHANGE_NOTIFICATION';
export const CHANGE_NOTIFICATION_SUCCESS = 'CHANGE_NOTIFICATION_SUCCESS'
export const CHANGE_NOTIFICATION_FAILED = 'CHANGE_NOTIFICATION_FAILED';


//type used for remember me checkbox in login screen
export const REMEMBER_ME_SUCCESS = 'REMEMBER_ME_SUCCESS'
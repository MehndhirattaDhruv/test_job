import * as auth from './auth';
import * as getEvents from './getEvents'
import * as pushNotifications from './pushNotifications'
import * as  internetCheck from './internetCheck';
import * as feedback from './feedback'
import * as giphyItems from './giphyItems'
export default {
  ...auth,
  ...getEvents,
  ...pushNotifications,
  ...internetCheck,
  ...feedback,
  ...giphyItems
};
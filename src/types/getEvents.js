//types used to fetch results of all events in homepage screen 
export const GET_MY_EVENTS = 'GET_MY_EVENTS';
export const GET_MY_EVENTS_SUCCESS = 'GET_MY_EVENTS_SUCCESS'
export const GET_MY_EVENTS_FAILED = 'GET_MY_EVENTS_FAILED'

//types used to fetch more results for all events 
export const FETCH_MORE_EVENTS = 'FETCH_MORE_EVENTS';
export const FETCH_MORE_EVENTS_SUCCESS = 'FETCH_MORE_EVENTS_SUCCESS'
export const FETCH_MORE_EVENTS_FAILED = 'FETCH_MORE_EVENTS_FAILED'

//types used in case of adding event by a logged in user 
export const POST_EVENTS = 'POST_EVENTS';
export const POST_EVENTS_SUCCESS = 'POST_EVENTS_SUCCESS'
export const POST_EVENTS_FAILED = 'POST_EVENTS_FAILED'

//type used to fetch my events that is the user's own event which he/she has posted
export const GET_OWN_EVENTS = 'GET_OWN_EVENTS';
export const GET_OWN_EVENTS_SUCCESS = 'GET_OWN_EVENTS_SUCCESS'
export const GET_OWN_EVENTS_FAILED = 'GET_OWN_EVENTS_FAILED'

//type used to fetch detail of event of one's own event 
export const GET_EVENT_DETAIL = 'GET_EVENT_DETAIL';
export const GET_EVENT_DETAIL_SUCCESS = 'GET_EVENT_DETAIL_SUCCESS'
export const GET_EVENT_DETAIL_FAILED = 'GET_EVENT_DETAIL_FAILED'

//type used to fetch detail of any event which is posted by anyone  
export const GET_EVENT_DETAIL_ALL = 'GET_EVENT_DETAIL_ALL';
export const GET_EVENT_DETAIL_SUCCESS_ALL = 'GET_EVENT_DETAIL_SUCCESS_ALL'
export const GET_EVENT_DETAIL_FAILED_ALL = 'GET_EVENT_DETAIL_FAILED_ALL'


// types to get list of subscribed events 
export const GET_SUBSCRIBED_EVENTS = 'GET_SUBSCRIBED_EVENTS';
export const GET_SUBSCRIBED_EVENTS_SUCCESS = 'GET_SUBSCRIBED_EVENTS_SUCCESS'
export const GET_SUBSCRIBED_EVENTS_FAILED = 'GET_SUBSCRIBED_EVENTS_FAILED'

//event history  get list of history events
export const GET_SUBSCRIBED_EVENTS_HISTORY = 'GET_SUBSCRIBED_EVENTS_HISTORY';
export const GET_SUBSCRIBED_EVENTS_HISTORY_SUCCESS = 'GET_SUBSCRIBED_EVENTS_HISTORY_SUCCESS'
export const GET_SUBSCRIBED_EVENTS_HISTORY_FAILED = 'GET_SUBSCRIBED_EVENTS_HISTORY_FAILED'

//used to get detail of the event history 
export const GET_SUBSCRIBED_EVENTS_HISTORY_DETAIL = 'GET_SUBSCRIBED_EVENTS_HISTORY_DETAIL';
export const GET_SUBSCRIBED_EVENTS_HISTORY_SUCCESS_DETAIL = 'GET_SUBSCRIBED_EVENTS_HISTORY_SUCCESS_DETAIL'
export const GET_SUBSCRIBED_EVENTS_HISTORY_FAILED_DETAIL = 'GET_SUBSCRIBED_EVENTS_HISTORY_FAILED_DETAIL'

//types used to edit event which user has created
export const EDIT_EVENT = 'EDIT_EVENT';
export const EDIT_EVENT_SUCCESS = 'EDIT_EVENT_SUCCESS'
export const EDIT_EVENT_FAILED = 'EDIT_EVENT_FAILED'

//types used to delete the  event which user has created
export const DELETE_EVENT = 'DELETE_EVENT';
export const DELETE_EVENT_SUCCESS = 'DELETE_EVENT_SUCCESS'
export const DELETE_EVENT_FAILED = 'DELETE_EVENT_FAILED'


//types used for subscribing  and unsubscribing the  event
export const SUBSCRIBE_POST_EVENT = 'SUBSCRIBE_POST_EVENT'
export const SUBSCRIBE_POST_EVENT_SUCCESS = 'SUBSCRIBE_POST_EVENT_SUCCESS'
export const SUBSCRIBE_POST_EVENT_FAILED = 'SUBSCRIBE_POST_EVENT_FAILED'


//type used to managing the organizer part in the add event and edit event section 
export const ADD_ORGANIZERS ='ADD_ORGANIZERS'

//type used to empty the organizers  
export const ADD_ORGANIZERS_EMPTY = 'ADD_ORGANIZERS_EMPTY'

//type used to handle logout to clear out all the previous events of the previous user
export const ON_LOGOUT = 'ON_LOGOUT'

export const SET_IS_LOADED = 'SET_IS_LOADED';

//used to fetch more results
export const GET_MY_EVENTS_LOAD_MORE = 'GET_MY_EVENTS_LOAD_MORE';

//type used to handle to have a track on scroll Index of all events 
export const SET_SCROLL_INDEX = 'SET_SCROLL_INDEX';
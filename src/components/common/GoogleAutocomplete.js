//File Name: GoogleAutocomplete.js
//Path: src/components/common
//Description: this component basically is used in case of  add events and edit events to fill the desired location of the event 
import React from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    Dimensions,
    TextInput,
    AsyncStorage,
    KeyboardAvoidingView,
    ScrollView,
    SafeAreaView,
    TouchableWithoutFeedback,
    keyboard,
    StatusBar,
    Alert,
    ImageBackground,
    Platform,
    BackHandler,
    Picker,
    FilePickerManager,
    TouchableOpacity,
    ActivityIndicator,
    Keyboard
} from 'react-native';

import {
    Content,
    Icon,
    Item,
    Input,
    Label,
    Form,
    Spinner,
    Header,
    Container,
    Footer,
    FooterTab,
    Left,
    Right,
    Body,
    Title,
    CardItem,
    Card,
    Textarea,
    DatePicker,
    Button
} from 'native-base';
import { connect } from 'react-redux'
import moment from 'moment';
import { GoogleAutoComplete } from 'react-native-google-autocomplete';


const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const today = new Date()

class GoogleAutocomplete extends React.Component {
    render() {  
        let { errors, editEvent ,editable  } = this.props;
        return (
            <GoogleAutoComplete apiKey='AIzaSyCZj40Co5f9FJF6rnkvYccVW1x-k3DgBDQ' debounce={500} minLength={3} queryTypes=''>
                        {(auto) => {
                            let { handleTextChange, locationResults, fetchDetails, isSearching, inputValue, clearSearchs } = auto;
                            return (
                                <React.Fragment>
                                    <View style={{ flex:1 , flexDirection:"row",alignItems:"center" , justifyContent:"center" ,alignContent:"center" , alignSelf:"center" , borderBottomWidth: editable && editEvent ||  errors.location  ? 0.9 : 0 , marginLeft:0.03*width , marginRight:0.03*width , borderBottomColor: errors && errors.location ? "#ed2f2f" : "#D9D5DC" }}>
                                       { editEvent ? <Image style={{height:20 , width:18 ,...styles.icon}} source={require('../../../assets/location.png')} /> :  null  }
                                      {editable ?   
                                      <Input
                                            style={{ flex:  editEvent ? 5 : 3  ,   ...styles.textInput}}
                                            placeholder="Select Location"
                                            onChangeText={this.props  && this.props.handleTextChange(handleTextChange)}
                                            value={this.props && this.props.data && this.props.data.location}
                                            disabled={this.props && this.props.editable ? false  : true}
                                      
                                        />:
                                            <Text style={{ flex: editEvent ? 5 : 3, ...styles.textInput }}>{this.props && this.props.data && this.props.data.location}</Text>
                                        }

                                       {editable  &&  <Button hitSlop={{top: 20, left: 20, bottom: 20, right: 20}} onPress={this.props && this.props.clearSearch(clearSearchs)} transparent>
                                            <Image style={{height:15 , width:15 , ...styles.icon}} source={require('../../../assets/cross_icon.png')} />
                                            </Button>
                                        }
                                    </View>
                                    {isSearching && <ActivityIndicator size="large" color="#5e2bce" />}
                                    <ScrollView keyboardShouldPersistTaps='always'>
                                        {locationResults.map(el => (
                                            <LocationItem
                                                {...el}
                                                key={el.id}
                                                fetchDetails={fetchDetails}
                                                update={this.props.update}
                                                {...{ clearSearchs }}
                                            />
                                        ))}
                                    </ScrollView>
                                </React.Fragment>
                            )
                        }}
                    </GoogleAutoComplete>
              
           

        )
    }
}


const styles = StyleSheet.create({
    textInput:{
        textAlign:"left",
        color: "#434343",
        fontSize: 15,
        paddingHorizontal: 0,
        paddingLeft: 0 
    },
    icon:{
        // height:20,
        // width:18,
        marginLeft:-3,
        resizeMode:"contain",
        marginRight:10
    },
    clearButton:{
        marginRight:0.03*width,
        alignContent:"center",
        alignSelf:"center",
        height:25,
        justifyContent:"center",
        borderRadius: 20,
        backgroundColor:"#dfd2fe" , 
         
    }
})


export default connect(state => state)(GoogleAutocomplete);



class LocationItem extends React.PureComponent {
    //FunctionName:_handlePress
    //Description:This function used is handle click on any location option to find out its description and latitude and longitude 

    _handlePress = async (item) => {
            Keyboard.dismiss()
            this.props.update(this.props.description || "");
             this.props.clearSearchs()
            const res = await this.props.fetchDetails(this.props.place_id)
            const { location  } = res && res.geometry || {}
            const latitude = location.lat
            const longitude = location.lng
            let latitudeLongitudeMaker = `${latitude},${longitude}`;
            this.props.update(this.props.description, latitudeLongitudeMaker || "")
    }

    render() {
        return (
            <TouchableOpacity style={{
                    height: 40,
                    borderBottomWidth: StyleSheet.hairlineWidth,
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                    backgroundColor: 'lightgray'

                }} onPress={() => this._handlePress(this.props.description)}>
                <Text style={{ marginLeft: 0.03*width , marginRight: 0.03 *  width }}>{this.props.description}</Text>
            </TouchableOpacity>
        )
    }
}

// apiKey='AIzaSyASVjXw-21Wwp_JahOuW-MVVnvhNh8J6Mk'

//blaze api key  =AIzaSyCZj40Co5f9FJF6rnkvYccVW1x-k3DgBDQ
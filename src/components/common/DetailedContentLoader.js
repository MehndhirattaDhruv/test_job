//File Name: DetailedContentLoader.js
//Path: src/components/common
//Description: this component uses to show conten loaders for the detail of the event
import React, { PureComponent } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Alert, Dimensions } from 'react-native';
import ContentLoader from 'rn-content-loader';
import { Rect, Circle } from 'react-native-svg'
import { Container, Footer, FooterTab, Button, Icon, Card, Content } from 'native-base';


const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

export default class DetailedContentLoader extends PureComponent {
    render() {
        return (
            <Container style={{ flex:1  , flexDirection:"column"}}>
                    <Card transparent style={styles.card} >
                        <ContentLoader>
                            <Rect height={0.28 * height} width={0.94 * width} rx='4' ry='4' />
                            <Rect y={0.32 * height} height={15} width={0.94 * width} rx='4' ry='4' />
                            <Rect y={0.36 * height} height={15} width={0.94 * width} rx='4' ry='4' />
                        </ContentLoader>
                    </Card>
                    <Card transparent style={{ flex: 1, marginLeft: 0.0392 * width,marginRight: 0.0392 * width}} >
                        <ContentLoader>
                            <Rect height={15} width={0.94 * width} rx='4' ry='4' />
                            <Rect y="30" height={15} width={0.94 * width} rx='4' ry='4' /> 
                            <Rect y="60" height={15} width={0.94 * width} rx='4' ry='4' />
                            <Rect y="90" height={15} width={0.94 * width} rx='4' ry='4' />
                            <Rect y="120" height={15} width={0.94 * width} rx='4' ry='4' />
                            <Rect y="150" height={15} width={0.94 * width} rx='4' ry='4' />
                            <Rect y="180" height={15} width={0.94 * width} rx='4' ry='4' />
                            <Rect y="210" height={15} width={0.94 * width} rx='4' ry='4' />
                            <Rect y="240" height={15} width={0.94 * width} rx='4' ry='4' />
                            <Rect y="270" height={15} width={0.94 * width} rx='4' ry='4' />
                        </ContentLoader>
                    </Card>
            </Container>
        );
    }
}



const styles = StyleSheet.create({
    footerIcon: {
        height: 25,
        width: 25,
        alignItems: "center",
        justifyContent: "center",
        resizeMode: "contain"
    },
    card: {
        flex: 1,
        marginLeft: 0.0392 * width,
        marginRight: 0.0392 * width,
        marginTop: 0.015 * height,
        paddingTop: 0.02 * height,
    }
})

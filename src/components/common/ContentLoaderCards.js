//File Name: ContentLoaderCards.js
//Path: src/components/common
//Description: this component is used to show content Loaders when the data is  taking its time to come from Backend 
import React, { PureComponent } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Alert, Dimensions } from 'react-native';
import ContentLoader from 'rn-content-loader';
import {Rect,Circle} from 'react-native-svg'
import { Container, Footer, FooterTab, Button, Icon,Card,Content } from 'native-base';
import { logout } from '../../../utils'

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

export default class ContentLoaderCards extends PureComponent {
    render() {
        return (
            <Container>
                <Content>
                {this.props && [...Array(this.props.count)].map((value,index)=>(
                        <Card  transparent style={styles.card} key={index} height={ 0.23*height} >
                        <ContentLoader>
                            <Rect height={0.19 * height} width={0.34 * width} rx='4' ry='4' />
                            <Rect x='160' y='5' rx='4' ry='4' width='120' height='10' />
                            <Rect x='160' y='30' rx='4' ry='4' width='220' height='10' />
                            <Rect x='160' y='50' rx='4' ry='4' width='220' height='10' />
                            <Rect x='160' y='75' rx='4' ry='4' width='220' height='10' />
                            <Rect x='160' y='100' rx='4' ry='4' width='220' height='10' />
                            <Rect x='160' y='125' rx='8' ry='8' width='80' height='20' />
                        </ContentLoader>

                    </Card>
                ))}
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
   footerIcon:{
       height: 25,
       width: 25,
       alignItems: "center",
       justifyContent: "center",
       resizeMode: "contain"
   },
   card:{
       marginLeft: 0.0392 * width,
       marginRight: 0.0392 * width,
       marginTop: 0.015 * height,
       marginBottom: 0.015 * height,
       paddingTop:0.02*height,
       paddingBottom:0.02*height
   } 
})

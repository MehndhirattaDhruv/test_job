//File Name: ContentLoaderCards.js
//Path: src/components/common
//Description: this component is used to show content Loaders when the data is  taking its time to come from Backend 
import React, { PureComponent } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Alert, Dimensions } from 'react-native';
import ContentLoader from 'rn-content-loader';
import {Rect,Circle} from 'react-native-svg'
import { Container, Footer, FooterTab, Button, Icon,Card,Content } from 'native-base';
import { logout } from '../../../utils'

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

export default class ContentLoaderAnalysis extends PureComponent {
    render() {
        return (
            <Container>
                <Content>
                {this.props && [...Array(this.props.count)].map((value,index)=>(
                        <Card  style={styles.card} key={index} height={0.25*height} >
                        <ContentLoader>
                            <Rect x='5' y='5' rx='4' ry='4' width={0.9*width} height='11' />
                            <Rect x='5' y='25' rx='4' ry='4' width='70' height='9' />
                            <Rect x='90' y='25' rx='4' ry='4' width='70' height='9' />
                            <Circle x="20" y="54" cx="20" cy="30" r="22"/>
                            <Circle x="120" y='54' cx="20" cy="30" r="22"/>
                            <Circle x="220" y='54' cx="20" cy="30" r="22"/>
                              <Rect x='20' y='120' rx='4' ry='4' width='40' height='9' />
                              <Rect x='120' y='120' rx='4' ry='4' width='40' height='9' />
                              <Rect x='220' y='120' rx='4' ry='4' width='40' height='9' />
                        </ContentLoader>

                    </Card>
                ))}
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
   footerIcon:{
       height: 25,
       width: 25,
       alignItems: "center",
       justifyContent: "center",
       resizeMode: "contain"
   },
   card:{
       marginLeft: 0.0392 * width,
       marginRight: 0.0392 * width,
       marginTop: 0.015 * height,
       marginBottom: 0.015 * height,
       paddingTop:0.02*height,
   } 
})

//File Name: LoadingImage.js
//Path: src/components/common
//Description: this component is used to show Loading image when image of card is not fully loaded on screen 
import React, { Fragment } from 'react';
import {
    Image,
    StyleSheet,
    ActivityIndicator,
    View,
    Platform
} from 'react-native';

export default class LoadingImage extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        }
    }

    //FunctionName:imageLoaded
    //Description:This function used to handle and show an dummy image until image from backend is loaded
    imageLoaded = () => {
        setTimeout(() => this.setState({
            loading: false
        }), 150);
    }
    
    render() {
        const { loading } = this.state;
        let { source, style } = this.props;
        return (
            <View>
                <Image
                    source={{ uri: source }}
                    onLoad={this.imageLoaded}
                    style={loading ? styles.hidden : style}
                />
                {loading ? <Image
                    source={require('../../../assets/placeholder.jpg')}
                    style={{ ...style }}
                    resizeMode="cover"
                /> : null}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    hidden: {
        width: null,
        height: Platform.OS == 'android' ? 0 : 0.5
    },
    wallpaper: {
        flex: 1,
        width: null,
        height: 300,
    },
});
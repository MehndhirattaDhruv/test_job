import {
    apiGet,
    apiPost,
    apiPut,
    apiDelete
} from '../../utils.js';

//api used to get list of all the events
export function getMyEventsAPI(search='', sort='', lat='' , long='' , start=0) {
    return apiGet(`/events?search=${search}&sort_by=${sort}&lat=${lat}&long=${long}&start=${start}`);
}

//api used for posting event
export function postEventsAPI (data ){
    return apiPost('/events', data)
}

//api used for the my event list
export function getOwnEventsAPI(){
    return apiGet('/me/events')
}

// api used for the detail of event
export function getEventDetailAPI(data){
    return apiGet(`/me/events`, data)
}

export function getEventDetailAllAPI(data) {
    return apiGet(`/events`, data)
}

// api used for editing the my events
export function editEventAPI(data, id){   
    return apiPut(`/events?event_id=${id.id}`, data)
}

export function deleteEventAPI(id){
    return apiDelete(`/events?event_id=${id}`)
}

//used for subscribe and unsubscribe the event

export function subscibeEventAPI (data){
    return apiPost(`/subscribe` , data)
}

//api  used to get all subscribed events
export function getSubscribeEventsAPI(){
    return apiGet(`/events/subscribed`);
}

//api used to get the list of event history
export function getSubscribedHistoryAPI (){
    return apiGet(`/events/subscribed?history`);
}

//api used to get detail of event history events
export function getDetailOfEventHistoryAPI (id){
    return apiGet(`/events/subscribed?history&id=${id}`);
}
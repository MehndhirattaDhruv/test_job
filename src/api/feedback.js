import {
    apiGet,
    apiPost,
    apiPut
} from '../../utils.js';

//api used for posting the feedback of the event history events which he/she had attended
export function postFeedbackAPI(data){
    return apiPost("/feedback", data)
}

//api used for posting app Reviews and feedback
export function postFeedbackAppAPI(data){
    return apiPost('/review' , data )
}
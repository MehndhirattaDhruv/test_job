
import {
    apiGet,
    apiPost,
    apiPut
} from '../../utils.js';

//api used for the push notification to send the notification to all the subscribers of the event 
export function pushNotificationsAPI(data) {
    return apiPost('/notification', data)
}


export function getUserAvailabilityAPI(data){
    return apiPost("/responseToNotification" , data)
}
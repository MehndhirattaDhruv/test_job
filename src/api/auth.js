import {
    apiGet,
    apiPost,
    apiPut
} from '../../utils.js';

//api used for login with email and password
export function loginAPI(user) {
    return apiPost('/login', user);
}

//api used for skip users to handle their sessions
export function skipLoginAPI(user) {
    return apiPost('/login/skip', user);
}

//api which handles the registration flow of the user 
export function registerAPI(user){
    return apiPost('/users', user)
}

//api which handle the forgotPassword functionality
export function forgotPasswordAPI(user){
    return apiPost('/forgotPassword' , user)
}

//api used for editing the profile of a loggedin user 
export function editProfileAPI(user){
    return apiPut('/users' , user)
}

//api used to handle the notification turn OFF and ON mechanism for allowing and disallowing the PUSH Notifications 
export function toggleNotificationAPI(){
    return apiGet("/changeNotificationStatus")
}
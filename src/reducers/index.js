export { default as auth} from './auth'
export { default as getEvents} from './getEvents'
export { default as pushNotifications} from './pushNotifications.js'
export { default as internetCheck} from './internetCheck';
export { default as feedback } from './feedback'
export { default as getGiphs } from './getGiphs'
import types from '../types';
import _ from 'lodash';

let initState = {
    fetching: false,
    events:[],
    myEvents:[],
    fetchingAddEvent:false,
    fetchingOwnEvents:false,
    eventObject:{},
    organizers:[],
    fetchingSubscribe:false,
    subscribedEvents:[],
    eventsHistory:[],
    total_Events:null,
    fetchingSubscribeEvents:false ,  // used to get fetching of all subscribe event detail
    laodMore: false,
    isLoaded: false,
    scrollIndex: 0
}

export default function (state = initState, action) {
    
    switch (action.type) {
        // reducer used to get all the event posted by everyone 
        case types.GET_MY_EVENTS:
            return { ...state, [action.payload.fetchingType]: true }
        case types.GET_MY_EVENTS_LOAD_MORE:
            const { events=[] } = state;
            let updatedEvents = [...events];
            updatedEvents = _.uniqBy([...events, ...(action.payload.events || [])], "id" );
            return { ...state, fetching: false, loadMore:false, events: updatedEvents}
        case types.GET_MY_EVENTS_SUCCESS:
            return { ...state, fetching: false, loadMore: false, events: [...action.payload.events] }
        case types.GET_MY_EVENTS_FAILED:
            return { ...state, fetching: false, loadMore: false }
        
        // reducer used to get one's own event posted by a logged in user  
        case types.GET_OWN_EVENTS:
            return { ...state, fetchingOwnEvents: true }
        case types.GET_OWN_EVENTS_SUCCESS:
            return { ...state, fetchingOwnEvents: false, myEvents: [...action.payload.events] }
        case types.GET_OWN_EVENTS_FAILED:
            return { ...state, fetchingOwnEvents: false }
        
        //reducer used to get list of subscribed events of the corresponding users
        case types.GET_SUBSCRIBED_EVENTS:
            return { ...state, fetchingSubscribeEvents: true, }
        case types.GET_SUBSCRIBED_EVENTS_SUCCESS:
            return { ...state, fetchingSubscribeEvents: false, subscribedEvents: [...action.payload.events] }
        case types.GET_SUBSCRIBED_EVENTS_FAILED:
            return { ...state, fetchingSubscribeEvents: false }

        //reducer used to get the list of event history of the corresponding user    
        case types.GET_SUBSCRIBED_EVENTS_HISTORY:
            return { ...state, fetching: true }
        case types.GET_SUBSCRIBED_EVENTS_HISTORY_SUCCESS:
            return { ...state, fetching: false, eventsHistory: [...action.payload.events] }
        case types.GET_SUBSCRIBED_EVENTS_HISTORY_FAILED:
            return { ...state, fetching: false }
        
        //reducer used for  posting an event 
        case types.POST_EVENTS:
            return { ...state, fetchingAddEvent: true }
        case types.POST_EVENTS_SUCCESS:
            return { ...state, fetchingAddEvent: false}
        case types.POST_EVENTS_FAILED:
            return { ...state, fetchingAddEvent: false } 

         //reducer used for  editing  an event 
        case types.EDIT_EVENT:
            return { ...state, fetchingAddEvent: true }
        case types.EDIT_EVENT_SUCCESS:
            return { ...state, fetchingAddEvent: false }
        case types.EDIT_EVENT_FAILED:
            return { ...state, fetchingAddEvent: false }    
        
         //reducer used for  deleting an event 
        case types.DELETE_EVENT:
            return { ...state, fetching: true }
        case types.DELETE_EVENT_SUCCESS:
            return { ...state, fetching: false }
        case types.DELETE_EVENT_FAILED:
            return { ...state, fetching: false }    

        //reducer used to get detail of one's own event detail for a logged in user  
        case types.GET_EVENT_DETAIL:
            return { ...state, fetching: true }
        case types.GET_EVENT_DETAIL_SUCCESS:
            return { ...state, fetching: false, eventObject:{...action.payload} }
        case types.GET_EVENT_DETAIL_FAILED:
            return { ...state, fetching: false } 
            
        //reducer used to get detail of event which are visible to everyone 
        case types.GET_EVENT_DETAIL_ALL:
            return { ...state, fetching: true }
        case types.GET_EVENT_DETAIL_SUCCESS_ALL:
            return { ...state, fetching: false, eventObject: { ...action.payload } }
        case types.GET_EVENT_DETAIL_FAILED_ALL:
            return { ...state, fetching: false }

        //reducer used to get detail of event which are in Event history TAB
        case types.GET_SUBSCRIBED_EVENTS_HISTORY_DETAIL:
            return { ...state, fetching: true }
        case types.GET_SUBSCRIBED_EVENTS_HISTORY_SUCCESS_DETAIL:
            return { ...state, fetching: false, eventObject: { ...action.payload } }
        case types.GET_SUBSCRIBED_EVENTS_HISTORY_FAILED_DETAIL:
            return { ...state, fetching: false }
     

        //reducer related to the subscribe and unsubscribe of the event     
        case types.SUBSCRIBE_POST_EVENT:
            return { ...state, fetchingSubscribe: true }
        case types.SUBSCRIBE_POST_EVENT_SUCCESS:
            return { ...state, fetchingSubscribe: false, }
        case types.SUBSCRIBE_POST_EVENT_FAILED:
            return { ...state, fetchingSubscribe: false }    

        //special case to  handle logout and clear all reducers and clear persistance of previous user 
        case types.ON_LOGOUT:
            return initState;

        //used to handle organizers add and delete it on mount of add event 
        case types.ADD_ORGANIZERS:
            return { ...state , organizers: [...action.payload]}  
        case types.ADD_ORGANIZERS_EMPTY:
            return { ...state, organizers: [] }       
            
        case types.SET_IS_LOADED:
            return { ...state, isLoaded: action.payload }

        //reducer used to handle case of maintaining the user scroll on list of  cards on which he has while he/she  is changing TABS     
        case types.SET_SCROLL_INDEX:
            return { ...state, scrollIndex: action.payload }

        default:
            return state

    }
}

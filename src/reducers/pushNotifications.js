import types from '../types';

let initState = {
    fetching: false,
}

export default function (state = initState, action) {

    switch (action.type) {
        //reducer used to send notification to the subscriber of the relevant event 
        case types.PUSH_NOTIFICATIONS:
            return { ...state, fetching: true }
        case types.PUSH_NOTIFICATIONS_SUCCESS:
            return { ...state, fetching: false}
        case types.PUSH_NOTIFICATIONS_FAILED:
            return { ...state, fetching: false }
        default:
            return state

    }
}

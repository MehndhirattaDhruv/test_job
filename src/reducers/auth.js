import types from '../types';

let initState = {
    fetching: false,
    fetchingProfile:false,
    user: {},
  
}

import { saveObject } from '../../utils'

export default function (state = initState, action) {
    
    switch (action.type) {
        //LOGIN
        case types.AUTH_LOGIN:
            return { ...state, fetching: true }
        case types.AUTH_LOGIN_SUCCESS:
            let newUser = { ...action.payload };
            saveObject(newUser)
            return { ...state, fetching: false, user: newUser }
        case types.AUTH_LOGIN_FAILED:
            return { ...state, fetching: false }

        //SKIP LOGIN
        case types.SKIP_LOGIN:
            return { ...state, fetching: true }
        case types.SKIP_LOGIN_SUCCESS:
            let skipUser = { ...action.payload };
            saveObject(skipUser)
            return { ...state, fetching: false, user: skipUser }
        case types.SKIP_LOGIN_FAILED:
            return { ...state, fetching: false }    

        //FORGOT PASSWORD
        case types.FORGOT_PASSWORD:
            return { ...state, fetching: true }
        case types.FORGOT_PASSWORD_SUCCESS:
            return { ...state, fetching: false }
        case types.FORGOT_PASSWORD_FAILED:
            return { ...state, fetching: false }

        //REGISTRATION
        case types.AUTH_REGISTRATION:
            return { ...state, fetching: true }
        case types.AUTH_REGISTRATION_SUCCESS:
            return { ...state, fetching: false }
        case types.AUTH_REGISTRATION_FAILED:
            return { ...state, fetching: false }
        
        //UPDATE_PROFILE
        case types.UPDATE_PROFILE:
            return { ...state, fetchingProfile: true }
        case types.UPDATE_PROFILE_SUCCESS:
            return { ...state, fetchingProfile: false }
        case types.UPDATE_PROFILE_FAILED:
            return { ...state, fetchingProfile: false }

        default:
            return { ...state }

    }
}

import types from '../types';

let initState = {
    fetching: false,
   
}


export default function (state = initState, action) {

    switch (action.type) {
        // reducer used for  post feedback of the event once he has attended that event
        case types.POST_REVIEW:
            return { ...state, fetching: true }
        case types.POST_REVIEW_SUCCESS:
            return { ...state, fetching: false }
        case types.POST_REVIEW_FAILED:
            return { ...state, fetching: false }

    // reducer used for  posting review or feedback of app by users
        case types.POST_REVIEW_FOR_APP:
            return { ...state, fetching: true }
        case types.POST_REVIEW_FOR_APP_SUCCESS:
            return { ...state, fetching: false }
        case types.POST_REVIEW_FOR_APP_FAILED:
            return { ...state, fetching: false }

        default:
            return { ...state }

    }
}

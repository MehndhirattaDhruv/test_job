import types from "../types";
const initial_state = {
	internetConnection: false
};

export default function(state = initial_state, actions) {
	switch (actions.type) {
		//reducer case used to handle internet checks 
		case types.CHECK_INTERNET_CONNECTION:
			let data = actions.payload;
            let internetConnection = true;
			if (data.type == "none") {
				internetConnection = false;
			}
			return { ...state, internetConnection };
	}
	return { ...state };
}

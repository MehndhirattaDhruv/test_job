import types from '../types';
import _ from 'lodash';

let initState = {
    fetching: false,
    gifArray:[]
}

export default function (state = initState, action) {
    
    switch (action.type) {
      
        case types.GET_GIPHY_ITEMS:
            return { ...state, fetching: true }
        case types.GET_GIPHY_ITEMS_SUCCESS:
            const { data =[] } = action.payload && action.payload.data
            const urlArray =  data && data.length ?  data.map((item)=> { return item && item.images && item.images.preview_gif && item.images.preview_gif.url })  : []
            return { ...state, fetching: false , gifArray: urlArray || [] }
        case types.GET_GIPHY_ITEMS_FAILED:
            return { ...state, fetching: false }

        default:
            return state

    }
}

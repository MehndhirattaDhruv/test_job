import React from 'react'
import axios from 'axios';
import {
    AsyncStorage
} from 'react-native';
import { apiUrl } from './src/constants'
import firebase from 'react-native-firebase';
export function isLoggedIn() {
    return AsyncStorage.getItem('user');
}

export function logout() {
    return AsyncStorage.removeItem('user');

}

/* export function getHeaders() {
    let session = getObject('session');
    return {
        authorization: session && session.accessToken || null
    }
} */

export function saveObject(value) {
    return AsyncStorage.setItem('user', JSON.stringify(value));
}

export function getObject(key) {

    return AsyncStorage.getItem(key)
}


export function generateUrl(path) {
    return apiUrl + path;
}

export function apiReq(endPoint, data, method, headers, config) {
    return new Promise(async (res, rej) => {
        try {
            let user = await getObject("user");
            headers = {
                ...headers,
                "Authorization": `Bearer ${user && JSON.parse(user).access_token || null}`,
                "Content-Type":"application/json"
            }


            if (method == 'get') {
                data = {
                    params: data,
                    headers,
                    ...config
                }
            }
            if (method == 'delete') {
                data = {
                    params: data,
                    headers,
                    ...config
                }
            }
            axios[method](endPoint, data, { ...config, headers }).then((result) => {

                let { data } = result;
                return res(data)
            }).catch((err) => {

                return rej(err);
            });

        }
        catch (err) {

        }
    })
}

export function apiPost(endPoint, data, headers = {}, config = {}) {
    return apiReq(generateUrl(endPoint), data, 'post', headers, config);
}

export function apiDelete(endPoint, data, headers = {}, config = {}) {
    return apiReq(generateUrl(endPoint), data, 'delete', headers, config);
}

export function apiGet(endPoint, data, headers = {}, config = {}) {
    return apiReq(generateUrl(endPoint), data, 'get', headers, config);
}

export function apiPut(endPoint, data, headers = {}, config = {}) {
    
    return apiReq(generateUrl(endPoint), data, 'put', headers, config);
}

export const generateError = (err) => {
    return err && err.response && err.response.data && err.response.data.message || 'Server Encountered an Error'; 
}

export async function checkPermission() {

    firebase.messaging().hasPermission().then(enabled => {
        if (enabled) {
            getToken();
        } else {
            requestPermission();
        }
    })

}

export function getToken() {

    getObject("fcm_token").then(storage => {

        let fcm_token = (storage && JSON.parse(storage)) || null;
        if (!fcm_token) {

            firebase.messaging().getToken().then(token => {
                saveFcmToken(token).then(res => {
               
                })
            })

            firebase.messaging().onTokenRefresh(token => {
                // alert("new token is genrated");
                saveFcmToken(token);
            });
        }
    });

}

export function requestPermission() {
    firebase.messaging().requestPermission().then(res => {
        getToken();
    }).catch(error => {
    })
}


export function saveFcmToken(value) {
    return AsyncStorage.setItem("fcm_token", JSON.stringify(value));
}
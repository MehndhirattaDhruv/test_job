import Validator from 'is_js';
import { isEmpty } from 'lodash';

export default function validateInputNotificationModal(data) {
    let errors = {};

    if (Validator.empty(data.messageNotification)) {
        errors.messageNotification = 'Notification messsage is required';
    }
  
    return {
        errors,
        isValidNotificationModal: isEmpty(errors)
    }
}

import Validator from 'is_js';
import { isEmpty } from 'lodash';

export default function validateInput(data) {
    let errors = {};

    if (Validator.empty(data.title)) {
        errors.title = 'Title is required';
    }

    if (Validator.empty(data.description)) {
        errors.description = 'Description is required';
    }
    if (Validator.empty(data.location)) {
        errors.location = 'Location is required';
    }
  
    return {
        errors,
        isValid: isEmpty(errors)
    }
}

import React from 'react';
import { Container, Root } from 'native-base';
import { Provider } from 'react-redux'
import store from './src/store'
import Routes from './src/containers/Routes';
import { YellowBox } from 'react-native';

export default class App extends React.Component {
  render() {
     console.disableYellowBox = true;
      // const _XHR = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest?
      //   GLOBAL.originalXMLHttpRequest :
      //   GLOBAL.XMLHttpRequest

      // XMLHttpRequest = _XHR
    return (
      <Root>
      <Container style={{ backgroundColor :"#fff"}}>
        <Provider store={store}>
          <Routes />
        </Provider>
      </Container>
      </Root>
    )
  }
}

